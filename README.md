# GraphTheoryExercises
This project is all of exercises about Graph Theory.

## Introduce:
This project implements algorithms to solve problems can be represented by a graph. Algorithms hadn't already optimized and there were few bugs. So that, You will be free to make a Pull request to merge your better idea into master branch.

I'm also looking for a Java GUI for this project, Please tell if You are interested in creating a beautiful GUI to make adjacent matrices and weight matrices easily to read

Thanks for your great idea and best regards.

## Clone:
$ git clone [https://github.com/Joseph-Maria/GraphTheoryExercises.git](https://github.com/Joseph-Maria/GraphTheoryExercises.git)
	
$ git clone [https://Joseph-Maria@bitbucket.org/Joseph-Maria/graphtheoryexercises.git](https://Joseph-Maria@bitbucket.org/Joseph-Maria/graphtheoryexercises.git)

## Algorithms had been implemented
* DFS
* BFS
* Back Tracking
* Kruskal
* Prim
* Dijkstra
* Ployd
* Bellman Ford
* Ford Fulkerson
	
## Warning:
If You are learning Graph Theory subject at Information Technology faculty of University of Agriculture and Forestry, You shall NOT copy this project as your homework. I won't take any responsibility for the consequence that you will be failed mid-term test.