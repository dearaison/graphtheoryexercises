package gui;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class UsingFileChooser {
	private static File currentDirectory = null;

	public static File getFileFromJFileChooser(boolean save) {
		File file;

		// create JFileChooser
		JFileChooser jFileChooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Text file", "txt");
		jFileChooser.setFileFilter(filter);

		// set JFileChooser to open current directory
		if (currentDirectory != null) {
			jFileChooser.setCurrentDirectory(currentDirectory);
		}
		int returnValue;

		if (save) {
			// set a default filename (this is where you default extension first comes in)
			jFileChooser.setSelectedFile(new File("new.txt"));
			// show open dialog and get the result of choice operation
			returnValue = jFileChooser.showSaveDialog(jFileChooser);
		} else {
			// show open dialog and get the result of choice operation
			returnValue = jFileChooser.showOpenDialog(jFileChooser);
		}

		// check whether we chose file or didn't.
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			// get the file and return it
			file = jFileChooser.getSelectedFile();
			currentDirectory = jFileChooser.getCurrentDirectory();
			return file;
		} else {
			System.out.println("Open command cancelled by user");
			return null;
		}
	}

}
