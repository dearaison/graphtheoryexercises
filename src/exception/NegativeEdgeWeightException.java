package exception;

public class NegativeEdgeWeightException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -143349516665582884L;

	/**
	 * @param negative
	 *            the negative number
	 * @param row
	 *            row index
	 * @param column
	 *            column index
	 */
	public NegativeEdgeWeightException(double negative, int row, int column) {
		super("Negative weight " + negative + " at row: " + row + " column: " + column);
	}
}