package exception;

public class NegativeCycleException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1787721478543958496L;

	/**
	 * @param negative
	 *            the negative number
	 * @param row
	 *            row index
	 * @param column
	 *            column index
	 */
	public NegativeCycleException() {
		super("There is a negative cycle in the graph's weight array");
	}
}
