package tree.tree;

import java.util.ArrayList;
import java.util.List;

import graph.connectedgraph.ConnectedGraphCheckable;
import graph.cyclegraph.CycleCheckable;
import graph.cyclegraph.DFSCycleGraphChecking;
import graph.datastructure.UndirectedGraph;
import tree.spanningtreealgorithms.MinimumSpanningTreeFindable;
import tree.spanningtreealgorithms.NormalSpanningTreeFindable;

public class Tree extends UndirectedGraph {
	private CycleCheckable cycleCheckAlgorithm = new DFSCycleGraphChecking();

	public Tree() {
	}

	public Tree(int[][] adjacencyMatrix) {
		super(adjacencyMatrix);
	}

	/**
	 * @param findWayAlgorithm
	 * @return
	 */
	public List<Integer> getAllCenter(ConnectedGraphCheckable findWayAlgorithm) {
		if (!this.isTree(cycleCheckAlgorithm)) {
			return null;
		}
		int numberOfVertices = this.getAdjacencyMatrix().length;
		int minEccentricity = Integer.MAX_VALUE;
		int[] eccentricities = new int[numberOfVertices];
		List<Integer> centerVertices = new ArrayList<Integer>();

		for (int u = 0; u < numberOfVertices; u++) {
			int eccentricityOfCurrentVertex = getEccentricityOfAVertex(u, findWayAlgorithm);
			eccentricities[u] = eccentricityOfCurrentVertex;
			if (minEccentricity > eccentricityOfCurrentVertex) {
				minEccentricity = eccentricityOfCurrentVertex;
			}
		}

		for (int i = 0; i < numberOfVertices; i++) {
			if (eccentricities[i] == minEccentricity) {
				centerVertices.add(i);
			}
		}
		return centerVertices;
	}

	/**
	 * Find eccentricity of a vertex
	 * 
	 * @param vertex
	 *            the vertex to find eccentricity
	 * @param findWayAlgorithm
	 *            algorithm to find way from vertex to all remain vertices, that way
	 *            as distance between two vertices
	 * @return the eccentricity of the vertex
	 */
	public int getEccentricityOfAVertex(int vertex, ConnectedGraphCheckable findWayAlgorithm) {
		if (!this.isTree(cycleCheckAlgorithm)) {
			return -1;
		}
		// initialize needing variable
		int numberOfVertices = this.getAdjacencyMatrix().length;
		int eccentricityOfAVertex = 0;

		for (int v = 0; v < numberOfVertices; v++) {
			if (v != vertex) {
				// get the length of the path which we can go from vertex to v
				int lengthOfPath = findWayAlgorithm.findWayBetweenTwoConnectedVertices(vertex, v, this).size() - 1;
				if (eccentricityOfAVertex < lengthOfPath) {
					eccentricityOfAVertex = lengthOfPath;
				}
			}
		}
		return eccentricityOfAVertex;
	}

	/**
	 * @param minimunSpanningTreeAlgorithm
	 * @param weightEgdes
	 * @return
	 */
	public Tree getMinimumFrameTree(MinimumSpanningTreeFindable minimunSpanningTreeAlgorithm,
			List<WeightEgde> weightEgdes) {
		return minimunSpanningTreeAlgorithm.findSpanningTree(this, weightEgdes);
	}

	/**
	 * @param findWayAlgorithm
	 * @return
	 */
	public int getRadius(ConnectedGraphCheckable findWayAlgorithm) {
		if (!this.isTree(cycleCheckAlgorithm)) {
			return -1;
		}
		int numberOfVertices = this.getAdjacencyMatrix().length;
		int radius = Integer.MAX_VALUE;
		for (int i = 0; i < numberOfVertices; i++) {
			int eccentricity = getEccentricityOfAVertex(i, findWayAlgorithm);
			if (radius > eccentricity) {
				radius = eccentricity;
			}
		}
		return radius;
	}

	/**
	 * @param FindTreeAlgorithm
	 * @return
	 */
	public Tree getSpanningTree(NormalSpanningTreeFindable FindTreeAlgorithm) {
		return FindTreeAlgorithm.findSpanningTree(this);
	}

	/**
	 * @param cycleCheckAlgorithm
	 *            the cycleCheckAlgorithm to set
	 */
	public void setCycleCheckAlgorithm(CycleCheckable cycleCheckAlgorithm) {
		this.cycleCheckAlgorithm = cycleCheckAlgorithm;
	}
}
