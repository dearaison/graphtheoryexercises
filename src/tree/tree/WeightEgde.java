package tree.tree;

public class WeightEgde implements Comparable<WeightEgde> {
	private int source;
	private int destination;
	private int weight;

	/**
	 * @param source
	 * @param destination
	 * @param weight
	 */
	public WeightEgde(int source, int destination, int weight) {
		super();
		this.source = source;
		this.destination = destination;
		this.weight = weight;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(WeightEgde that) {
		return this.weight - that.weight;
	}

	/**
	 * @return the destination
	 */
	public int getDestination() {
		return destination;
	}

	/**
	 * @return the source
	 */
	public int getSource() {
		return source;
	}

	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}

}
