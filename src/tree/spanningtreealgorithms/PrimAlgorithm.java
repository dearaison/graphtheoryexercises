package tree.spanningtreealgorithms;

import java.util.ArrayList;
import java.util.List;

import graph.datastructure.UndirectedGraph;
import tree.tree.Tree;
import tree.tree.WeightEgde;

public class PrimAlgorithm implements MinimumSpanningTreeFindable {

	private WeightEgde findMinimumWeightDelta(boolean[] isInTree, List<WeightEgde> weights) {
		List<WeightEgde> deltas = getDeltaEdge(isInTree, weights);
		WeightEgde delta = deltas.get(0);
		int minWeight = Integer.MAX_VALUE;

		for (WeightEgde weightEgde : deltas) {
			if (weightEgde.getWeight() < minWeight) {
				minWeight = weightEgde.getWeight();
				delta = weightEgde;
			}
		}
		return delta;
	}

	@Override
	public Tree findSpanningTree(UndirectedGraph undirectedGraph, List<WeightEgde> weightEgdes) {
		List<WeightEgde> copyOfWeight = new ArrayList<WeightEgde>();
		copyOfWeight.addAll(weightEgdes);

		int numberOfVertices = undirectedGraph.getAdjacencyMatrix().length;
		Tree result = new Tree(new int[numberOfVertices][numberOfVertices]);

		// boolean matrix to mark whether a vertex is in Tree or not
		boolean[] isInTree = new boolean[numberOfVertices];

		isInTree[0] = true;

		int numberOfTreeEdges = numberOfVertices - 1;
		int count = 0;
		while (count < numberOfTreeEdges) {
			// get a edge which has a vertex in Tree and other one out of Tree
			WeightEgde delta = findMinimumWeightDelta(isInTree, copyOfWeight);

			int source = delta.getSource();
			int destination = delta.getDestination();

			// add edge between two vertices
			result.addEdge(source, destination, 1);

			// Remove edge
			copyOfWeight.remove(delta);

			// mark two vertices of delta is visited
			isInTree[source] = isInTree[destination] = true;
			count++;
		}
		return result;
	}

	private List<WeightEgde> getDeltaEdge(boolean[] isInTree, List<WeightEgde> weights) {
		List<WeightEgde> deltas = new ArrayList<WeightEgde>();
		for (WeightEgde weightEgde : weights) {
			// check edge which has a vertex is in tree and other one is out
			if (isInTree[weightEgde.getSource()] != isInTree[weightEgde.getDestination()]) {
				deltas.add(weightEgde);
			}
		}
		return deltas;
	}

}
