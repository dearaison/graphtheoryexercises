package tree.spanningtreealgorithms;

import java.util.List;

import graph.datastructure.UndirectedGraph;
import tree.tree.Tree;
import tree.tree.WeightEgde;

/** This interface consist all of algorithms to find minimum spanning tree */
public interface MinimumSpanningTreeFindable {

	/**
	 * Search in all of edge to find a minimum spanning tree
	 * 
	 * @param undirectedGraph a UndirectedGraph
	 *            which to find the spanning tree
	 * @param weightEgdes
	 *            Weight of each edge in the graph
	 * @return the minimum spanning tree
	 */
	public Tree findSpanningTree(UndirectedGraph undirectedGraph, List<WeightEgde> weightEgdes);
}
