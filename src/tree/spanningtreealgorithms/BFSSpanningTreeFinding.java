package tree.spanningtreealgorithms;

import java.util.concurrent.ConcurrentLinkedQueue;

import graph.datastructure.UndirectedGraph;
import tree.tree.Tree;

public class BFSSpanningTreeFinding implements NormalSpanningTreeFindable {

	@Override
	public Tree findSpanningTree(UndirectedGraph undirectedGraph) {
		int numberOfVertices = undirectedGraph.getAdjacencyMatrix().length;
		int[][] matrix = undirectedGraph.getAdjacencyMatrix();
		Tree result = new Tree(new int[numberOfVertices][numberOfVertices]);

		// Create a stack (FIFO) of vertex numbers and
		// enqueue source vertex for BFS traversal
		ConcurrentLinkedQueue<Integer> verticesQueue = new ConcurrentLinkedQueue<Integer>();
		boolean[] visited = new boolean[numberOfVertices];

		verticesQueue.add(0);
		visited[0] = true;
		int count = 1;

		int u;
		while (!verticesQueue.isEmpty() && count < numberOfVertices) {
			u = verticesQueue.poll();
			for (int v = 0; v < numberOfVertices; v++) {
				if (!visited[v] && matrix[u][v] > 0) {
					visited[v] = true;
					verticesQueue.add(v);
					result.addEdge(u, v, 1);
					count++;
				}
			}
		}
		return result;
	}

}
