package tree.spanningtreealgorithms;

import graph.datastructure.UndirectedGraph;
import tree.tree.Tree;

public class DFSRecursiveSpanningTreeFinding implements NormalSpanningTreeFindable {
	private int numberOfVertices;
	private int[][] adjacencyMatrix;
	private boolean[] visited;

	private void expandTree(int u, Tree tree) {
		for (int v = 0; v < this.numberOfVertices; v++) {
			if (this.adjacencyMatrix[u][v] > 0 && !this.visited[v]) {
				tree.addEdge(u, v, 1);
				this.visited[v] = true;
				expandTree(v, tree);
			}
		}
	}

	@Override
	public Tree findSpanningTree(UndirectedGraph undirectedGraph) {
		this.numberOfVertices = undirectedGraph.getAdjacencyMatrix().length;
		this.adjacencyMatrix = undirectedGraph.getAdjacencyMatrix();
		
		this.visited = new boolean[this.numberOfVertices];
		Tree result = new Tree(new int[numberOfVertices][numberOfVertices]);

		visited[0] = true;

		expandTree(0, result);
		return result;
	}

}
