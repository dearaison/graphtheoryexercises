package tree.spanningtreealgorithms;

import java.util.Stack;

import graph.datastructure.UndirectedGraph;
import tree.tree.Tree;

public class DFSNonRecursiveSpanningTreeFinding implements NormalSpanningTreeFindable {
	private int numberOfVertices;
	private int[][] adjacencyMatrix;
	private boolean[] visited;

	@Override
	public Tree findSpanningTree(UndirectedGraph undirectedGraph) {
		// initialize variable
		numberOfVertices = undirectedGraph.getAdjacencyMatrix().length;
		adjacencyMatrix = undirectedGraph.getAdjacencyMatrix();
		visited = new boolean[numberOfVertices];

		// Stack contain vertices which were visited
		Stack<Integer> visitedStack = new Stack<Integer>();
		Tree result = new Tree(new int[numberOfVertices][numberOfVertices]);

		int u = 0;
		visited[u] = true;
		int count = 1;

		boolean stop = false;
		mainLoop: while (!stop && count < numberOfVertices) {
			stop = true;

			// find a vertex which adjacent with u and still not visited
			for (int v = 0; v < numberOfVertices; v++) {
				if (!visited[v] && adjacencyMatrix[u][v] > 0) {
					result.addEdge(u, v, 1);
					visitedStack.push(u);

					u = v;
					visited[v] = true;
					count++;
					stop = false;
					continue mainLoop;
				}
			}

			if (stop && !visitedStack.isEmpty()) {
				u = visitedStack.pop();
				stop = false;
			}
		}
		return result;
	}

}
