package tree.spanningtreealgorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import graph.connectedgraph.ConnectedGraphCheckable;
import graph.connectedgraph.DFSCheckingConnectedGraph;
import graph.datastructure.UndirectedGraph;
import tree.tree.Tree;
import tree.tree.WeightEgde;

public class KruskalAlgorithm implements MinimumSpanningTreeFindable {

	@Override
	public Tree findSpanningTree(UndirectedGraph graph, List<WeightEgde> weightEgdes) {
		ConnectedGraphCheckable connectedCheckor = new DFSCheckingConnectedGraph();

		List<WeightEgde> copyOfWeight = new ArrayList<WeightEgde>();
		copyOfWeight.addAll(weightEgdes);

		Collections.sort(copyOfWeight);
		int numberOfVertices = graph.getAdjacencyMatrix().length;
		Tree result = new Tree(new int[numberOfVertices][numberOfVertices]);

		// the number of spanning tree edges
		int treeEdges = numberOfVertices - 1;

		// count the number of edges which have been add into tree
		int countEdges = 0;
		for (WeightEgde weightEgde : copyOfWeight) {
			if (countEdges == treeEdges) {
				break;
			}
			int source = weightEgde.getSource();
			int destination = weightEgde.getDestination();
			if (!graph.isConnected(source, destination, connectedCheckor)) {
				result.addEdge(source, destination, 1);
				countEdges++;
			}
		}
		return result;
	}

}
