package tree.spanningtreealgorithms;

import graph.datastructure.UndirectedGraph;
import tree.tree.Tree;

/**
 * Interface consist all of method are used for finding spanning tree from a
 * graph
 */
public interface NormalSpanningTreeFindable {

	/**
	 * Find a spanning tree
	 * 
	 * @param undirectedGraph
	 *            a undirectedGraph to find a spanning tree
	 * @return a spanning tree if it exists
	 */
	public Tree findSpanningTree(UndirectedGraph undirectedGraph);
}
