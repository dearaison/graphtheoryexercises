package graph.maxflow;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import graph.datastructure.DirectedGraph;

public final class FordFulkersonAlgorithm implements MaxFlowInterface {
	private int numberOfVertices;

	private double[][] flow;
	private double[][] capacity;

	private int source;
	private int sink;

	// to check whether a vertex was labeled or not yet
	private boolean[] labeled;
	// to contain all vertices which were labeled but not yet scanned
	private Queue<Integer> notYetScanned;

	private int[] previousVertex;
	private double[] delta;

	private int maxFlowValue;

	/**
	 * Constructor.
	 * 
	 * @param graph
	 * @param source
	 * @param sink
	 */
	public FordFulkersonAlgorithm(DirectedGraph graph, int source, int sink) {
		if (source == sink)
			throw new IllegalArgumentException("Source and sink can't be same.");

		this.numberOfVertices = graph.getWeight().length;

		this.flow = new double[numberOfVertices][numberOfVertices];
		this.capacity = graph.getWeight();

		this.source = source;
		this.sink = sink;
		this.maxFlowValue = 0;

		runFordFulkerson();
	}

	private void dfs(int currentVertex, boolean[] visited) {
		visited[currentVertex] = true;
		for (int v = 0; v < numberOfVertices; v++) {
			if (flow[currentVertex][v] > 0 && !visited[v]) {
				dfs(v, visited);
			}
		}
	}

	/**
	 * Find a path with can increase the flow of network.
	 * <p>
	 * Breath First Search is used for traversing.
	 * 
	 * @return true if there a exist a path to increase network flow, false
	 *         otherwise
	 */
	private boolean findFlowIncreasingPath() {
		labeled = new boolean[numberOfVertices];
		// there is only sources vertex is labeled, and others are not
		labeled[source] = true;

		notYetScanned = new LinkedList<Integer>();
		// source vertex is labeled but it has not already scanned
		notYetScanned.add(source);

		previousVertex = new int[numberOfVertices];
		previousVertex[source] = -1;

		delta = new double[numberOfVertices];
		delta[source] = Double.POSITIVE_INFINITY;

		while (!notYetScanned.isEmpty()) {
			int u = notYetScanned.poll();

			for (int v = 0; v < numberOfVertices; v++) {
				// make sure v vertex was not labeled
				if (!labeled[v]) {
					boolean label = false;
					// check whether the the edge is from u to v
					// and Load deviation of that edge is greater than zero
					if (capacity[u][v] > 0 && capacity[u][v] > flow[u][v]) {
						previousVertex[v] = u;
						delta[v] = Math.min(delta[u], capacity[u][v] - flow[u][v]);

						label = true;
					}

					// check whether the the edge is from v to u (returned edge)
					// and flow of that edge is greater than zero
					if (capacity[v][u] > 0 && flow[v][u] > 0) {
						previousVertex[v] = -u; // assign returned edge
						delta[v] = Math.min(delta[u], flow[u][v]);

						label = true;
					}

					if (label) {
						labeled[v] = true;
						notYetScanned.add(v);
					}
				}
			}
		}
		return labeled[sink];
	}

	@Override
	public double[][] getFlow() {
		return flow;
	}

	@Override
	public int getMaxFlowValue() {
		return maxFlowValue;
	}

	@Override
	public List<List<Integer>> getMinCut() {
		List<Integer> reachable = new ArrayList<Integer>();
		List<Integer> unReachable = new ArrayList<Integer>();

		List<List<Integer>> result = new ArrayList<List<Integer>>();
		result.add(reachable);
		result.add(unReachable);

		boolean[] visited = new boolean[numberOfVertices];
		dfs(source, visited);

		for (int i = 0; i < numberOfVertices; i++) {
			for (int j = 0; j < numberOfVertices; j++) {
				if (capacity[i][j] > 0 && visited[i] && !visited[j]) {
					reachable.add(i);
					unReachable.add(j);
				}

			}
		}
		return result;
	}

	/**
	 * Increase network flow with the increasing path.
	 */
	private void increaseFlow() {
		// FIXME
		int v = sink, u = previousVertex[v];
		double increment = delta[sink];
		maxFlowValue += increment;

		while (v != source) {
			if (u > 0) {
				flow[u][v] += increment;
			} else {
				flow[Math.abs(u)][v] -= increment;
			}

			v = u;
			u = previousVertex[v];
		}
	}

	private void runFordFulkerson() {
		boolean stop = false;
		while (!stop) {
			if (findFlowIncreasingPath())
				increaseFlow();
			else
				stop = true;
		}
	}

}
