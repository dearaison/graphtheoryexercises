package graph.maxflow;

import java.util.List;

public interface MaxFlowInterface {
	/**
	 * @return max flow of the network
	 */
	public double[][] getFlow();

	/**
	 * @return max flow of data is transfer from source to sink
	 */
	public int getMaxFlowValue();

	/**
	 * @return minimum cut of the network
	 */
	public List<List<Integer>> getMinCut();
}
