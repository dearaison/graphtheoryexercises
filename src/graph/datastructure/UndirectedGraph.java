package graph.datastructure;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import graph.bipartilegraph.BipartiteGraphCheckable;
import graph.connectedgraph.ConnectedGraphCheckable;
import graph.cyclegraph.CycleCheckable;
import graph.searchalgorithms.Searchable;
import graph.shortestpathfinding.Dijkstra;
import graph.shortestpathfinding.Ployd;

/**
 * This class is subclass of Graph. This class also consist of undirected
 * graph's method which is not defined in Graph.
 */

public class UndirectedGraph extends Graph {

	/**
	 * 
	 */
	public UndirectedGraph() {
		super();
	}

	/**
	 * @param weight
	 */
	public UndirectedGraph(double[][] weight) {
		super(weight);
	}

	/**
	 * @param adjacencyMatrix
	 */
	public UndirectedGraph(int[][] adjacencyMatrix) {
		super(adjacencyMatrix);
	}

	/**
	 * @param adjacencyMatrix
	 * @param weight
	 */
	public UndirectedGraph(int[][] adjacencyMatrix, double[][] weight) {
		super(adjacencyMatrix, weight);
	}

	@Override
	public void addEdge(int firstVertex, int secondVertex, int numberOfEdges) {
		this.adjacencyMatrix[firstVertex][secondVertex] += numberOfEdges;
		this.adjacencyMatrix[secondVertex][firstVertex] += numberOfEdges;
	}

	@Override
	public void addEdgeWeight(int firstVertex, int secondVertex, double weight) {
		this.weight[firstVertex][secondVertex] = weight;
		this.weight[secondVertex][firstVertex] = weight;
	}

	@Override
	public List<Integer> breadthFirstSearch(Searchable searchAlgorithm) {
		// the number of vertices
		int numberOfVertices = this.adjacencyMatrix.length;
		int beginningVertex = 0;

		// List of visited vertices
		ArrayList<Integer> visitedList = new ArrayList<Integer>();

		// This code is to handle disconnected graph
		while (visitedList.size() < numberOfVertices) {
			// if the beginning vertex is visited, we won't traverse from it.

			// That because when a vertex was visited once time, then all
			// vertices
			// which connect with that vertex also were visited
			if (!visitedList.contains(beginningVertex))
				searchAlgorithm.breadthFirstSearch(beginningVertex, this, visitedList);
			beginningVertex++;
		}
		System.out.println(visitedList.toString());
		return visitedList;
	}

	public DirectedGraph converToDirectedGraph() {
		return new DirectedGraph(this.adjacencyMatrix, this.weight);
	}

	@Override
	public void deleteEdge(int firstVertex, int secondVertex) {
		this.adjacencyMatrix[firstVertex][secondVertex]--;
		this.adjacencyMatrix[secondVertex][firstVertex]--;
	}

	@Override
	public List<Integer> depthFirstSearch(Searchable searchAlgorithm) {
		// the number of vertices
		int numberOfVertices = this.adjacencyMatrix.length;
		int beginningVertex = 0;

		// List of visited vertices
		ArrayList<Integer> visitedList = new ArrayList<Integer>();

		// This code is to handle disconnected graph
		while (visitedList.size() < numberOfVertices) {
			// if the beginning vertex is visited, we won't traverse from it.

			// That because when a vertex was visited once time, then all
			// vertices
			// which connect with that vertex also were visited
			if (!visitedList.contains(beginningVertex))
				searchAlgorithm.depthFirstSearch(beginningVertex, this, visitedList);
			beginningVertex++;
		}
		System.out.println(visitedList.toString());
		return visitedList;
	}

	@Override
	public Queue<Integer> findShortestPathUsingDijkstra(int start, int end, Dijkstra dijkstra) {
		return dijkstra.getShortestPath(start, end);
	}

	@Override
	public Queue<Integer> findShortestPathUsingPloyd(int start, int end, Ployd ployd) {
		return ployd.getShortestPath(start, end);
	}

	@Override
	public List<List<Integer>> getConnectedComponents(Searchable searchAlgorithm) {
		// the method result
		List<List<Integer>> connectedComponentsList = new ArrayList<>();
		// this list temporarily store all of vertices of a connected component
		ArrayList<Integer> aConnectedComponent;
		// List of visited vertices
		ArrayList<Integer> visitedList = new ArrayList<Integer>();

		// the number of vertices
		int numberOfVertices = this.adjacencyMatrix.length;
		int beginningVertex = 0;

		// This code is to handle disconnected graph
		while (visitedList.size() < numberOfVertices) {
			// if the beginning vertex is visited, we won't traverse from it.

			// That because when a vertex was visited once time, then all
			// vertices
			// which connect with that vertex also were visited
			if (!visitedList.contains(beginningVertex)) {
				searchAlgorithm.depthFirstSearch(beginningVertex, this, visitedList);
				aConnectedComponent = new ArrayList<>();
				// Traversing from the first vertex to the last vertex of
				// current connected
				// component
				for (int i = visitedList.indexOf(beginningVertex); i < visitedList.size(); i++) {
					// Add all vertices of current connected components into
					// aConnectedComponent
					aConnectedComponent.add(visitedList.get(i));
				}
				// Add the connected component into result list
				connectedComponentsList.add(aConnectedComponent);
			}
			beginningVertex++;
		}
		System.out.println(connectedComponentsList.toString());
		return connectedComponentsList;
	}

	@Override
	public int getDegree(int vertexIndex) {
		int degree = 0;
		int numberOfVertices = this.adjacencyMatrix.length;
		for (int i = 0; i < numberOfVertices; i++) {
			degree += this.adjacencyMatrix[vertexIndex][i];
		}
		return degree;
	}

	@Override
	public int getNumberOfConnectedComponents(Searchable searchAlgorithm) {
		int result = 0;
		// the number of vertices
		int numberOfVertices = this.adjacencyMatrix.length;
		int beginningVertex = 0;

		// List of visited vertices
		ArrayList<Integer> visitedList = new ArrayList<Integer>();

		// This code is to handle disconnected graph
		while (visitedList.size() < numberOfVertices) {
			// if the beginning vertex is visited, we won't traverse from it.

			// That because when a vertex was visited once time, then all
			// vertices
			// which connect with that vertex also were visited
			if (!visitedList.contains(beginningVertex)) {
				searchAlgorithm.depthFirstSearch(beginningVertex, this, visitedList);
				result++;
			}
			beginningVertex++;
		}
		return result;
	}

	@Override
	public int getNumberOfEdges() {
		int numberOfEdges = 0;
		int numberOfVertices = this.adjacencyMatrix.length;
		for (int i = 0; i < numberOfVertices; i++) {
			for (int j = 0; j < numberOfVertices; j++) {
				numberOfEdges += this.adjacencyMatrix[i][j];
			}
		}
		numberOfEdges /= 2;
		return numberOfEdges;
	}

	/**
	 * Check the graph is bipartite or not
	 * 
	 * @param bipartiteCheckingAlgorithm
	 *            the checking algorithm to use
	 * @return true if the graph is bipartite, false otherwise
	 */
	public boolean isBipartite(BipartiteGraphCheckable bipartiteCheckingAlgorithm) {
		// the number of vertices
		int numberOfVertices = this.adjacencyMatrix.length;

		// This code is to handle disconnected graph
		for (int i = 0; i < numberOfVertices; i++)
			if (!bipartiteCheckingAlgorithm.isBipartite(i, this))
				return false;

		return true;
	}

	/**
	 * Check the graph is complete bipartite or not. Complete bipartite graphs must
	 * satisfy all of three conditions: has only one connected component; is a
	 * bipartite graph; is a complete graph
	 * 
	 * @param startVertices
	 *            the vertex where we start the traverse operation
	 * @param connectedCheckingAlgorithm
	 *            the algorithm to check whether the graph is connected or not
	 * @param bipartiteCheckingAlgorithm
	 *            the algorithm to check whether the graph is bipartite or not
	 * @return true if and only if the graph is complete bipartite, false otherwise
	 */
	public boolean isCompleteBipartite(int startVertices, ConnectedGraphCheckable connectedCheckingAlgorithm,
			BipartiteGraphCheckable bipartiteCheckingAlgorithm) {
		// check connected
		if (!this.isConnected(connectedCheckingAlgorithm))
			return false;
		// the array contain two color are used for marking vertices which are
		// decomposed
		// into two disjoint sets.
		int[] colorArray = new int[this.adjacencyMatrix.length];
		// check bipartite
		if (!bipartiteCheckingAlgorithm.isBipartite(startVertices, this, colorArray))
			return false;
		// decompose graph's vertices into two lists
		ArrayList<Integer> firstColorVertices = new ArrayList<Integer>();
		ArrayList<Integer> secondColorVertices = new ArrayList<Integer>();
		for (int i = 0; i < colorArray.length; i++) {
			if (colorArray[i] < 0)
				firstColorVertices.add(i);
			else
				secondColorVertices.add(i);
		}
		int numberOfFirstColorVertices = firstColorVertices.size();
		int numberOfSecondColorVertices = secondColorVertices.size();
		// Check each vertex in first list is connected to all of vertices in
		// second
		// list
		for (int i = 0; i < numberOfFirstColorVertices; i++) {
			if (this.getDegree(firstColorVertices.get(i)) != numberOfSecondColorVertices)
				return false;
		}
		// Check each vertex in second list is connected to all of vertices in
		// first
		// list
		for (int i = 0; i < numberOfSecondColorVertices; i++) {
			if (this.getDegree(secondColorVertices.get(i)) != numberOfFirstColorVertices)
				return false;
		}
		return true;
	}

	@Override
	public boolean isConnected(ConnectedGraphCheckable connectedCheckingAlgorithm) {
		return connectedCheckingAlgorithm.countConnectedVertices(0, this) == this.adjacencyMatrix.length;
	}

	@Override
	public boolean isConnected(int firstVertex, int secondVertex, ConnectedGraphCheckable connectedCheckingAlgorithm) {
		return connectedCheckingAlgorithm.isTwoVerticesConnected(firstVertex, secondVertex, this);
	}

	@Override
	public boolean isEuler(ConnectedGraphCheckable connectedCheckingAlgorithm) {
		// if the graph is not connected, it won't be a Euler graph
		if (!this.isConnected(connectedCheckingAlgorithm)) {
			return false;
		}
		int numberOfVertices = this.adjacencyMatrix.length;
		for (int i = 0; i < numberOfVertices; i++) {
			// if there is only one vertex has odd degree, it won't be a Euler
			// graph
			if ((this.getDegree(i) % 2) != 0) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isHalfEuler(ConnectedGraphCheckable connectedCheckingAlgorithm) {
		// if the graph is not connected, it won't be a Euler graph
		if (!this.isConnected(connectedCheckingAlgorithm)) {
			return false;
		}
		int numberOfVertices = this.adjacencyMatrix.length;
		// The number of vertices which have odd degree
		int count = 0;
		for (int i = 0; i < numberOfVertices; i++) {
			// if vertex has odd degree, we shall increase count variable by
			// one.
			if ((this.getDegree(i) % 2) != 0) {
				count++;
			}
		}
		// the graph will be half Euler graph,
		// if and only if the graph is Euler graph or
		// there are only two vertices which has odd degree,
		// the graph will be a Euler graph
		return (count == 0 || count == 2) ? true : false;
	}

	@Override
	public boolean isHalfEuler(int[] oddVertex, ConnectedGraphCheckable connectedCheckingAlgorithm) {
		// if the graph is not connected, it won't be a Euler graph
		if (!this.isConnected(connectedCheckingAlgorithm)) {
			return false;
		}
		int numberOfVertices = this.adjacencyMatrix.length;
		// The number of vertices which have odd degree
		int count = 0;
		for (int i = 0; i < numberOfVertices; i++) {
			// if vertex has odd degree, we shall increase count variable by
			// one.
			if ((this.getDegree(i) % 2) != 0) {
				count++;
				oddVertex[0] = i;
			}
		}
		// the graph will be half Euler graph,
		// if and only if the graph is Euler graph or
		// there are only two vertices which has odd degree,
		// the graph will be a Euler graph
		return (count == 0 || count == 2) ? true : false;
	}

	/**
	 * Check whether the graph is tree or not.
	 * 
	 * @param checkCycleAlgorithm
	 *            a object is used for checking a graph contain cycle or not
	 * @return true if and only if the graph is tree, false otherwise
	 */
	public boolean isTree(CycleCheckable checkCycleAlgorithm) {
		boolean[] visitedVertices = new boolean[this.adjacencyMatrix.length];
		// if the graph have cycle it won't be a tree
		if (checkCycleAlgorithm.isCycle(this, visitedVertices)) {
			return false;
		}
		for (boolean visited : visitedVertices) {
			if (!visited) {
				return false;
			}
		}

		return true;
	}
}
