package graph.datastructure;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;

import graph.connectedgraph.ConnectedGraphCheckable;
import graph.eulerianpathalgorithms.EulerianPathSearchable;
import graph.hamiltoncircuitalgorithms.HamiltonianCircuitSearchable;
import graph.searchalgorithms.Searchable;
import graph.shortestpathfinding.Dijkstra;
import graph.shortestpathfinding.Ployd;
import gui.UsingFileChooser;

/*
 * teacher's email: tqv@hcmuaf.edu.vn
 * 
 * subject: LTDT01/HK171/Ca01
 * 
 * file : <masv>_<ho ten khong dau tieng viet>.rar
 * */
/**
 * This class define a data structure for graph. This class implement almost
 * algorithms for graph.
 * 
 * @author Joseph Maria
 */

public abstract class Graph {
	private static int[][] createAdjacencyMatrixFromWeight(double[][] weight) {
		int[][] adjacencyMatrix = new int[weight.length][weight.length];
		for (int i = 0; i < weight.length; i++) {
			for (int j = 0; j < weight.length; j++) {
				if (Double.isFinite(weight[i][j])) {
					adjacencyMatrix[i][j]++;
				}
			}
		}
		return adjacencyMatrix;
	}
	protected int[][] adjacencyMatrix;

	protected double[][] weight;

	/**
	 * 
	 */
	public Graph() {
		super();
	}

	/**
	 * @param weight
	 */
	public Graph(double[][] weight) {
		super();
		this.weight = weight;
		this.adjacencyMatrix = createAdjacencyMatrixFromWeight(weight);
	}

	/**
	 * @param adjacencyMatrix
	 */
	public Graph(int[][] adjacencyMatrix) {
		super();
		this.adjacencyMatrix = adjacencyMatrix;
	}

	/**
	 * @param adjacencyMatrix
	 * @param weight
	 */
	public Graph(int[][] adjacencyMatrix, double[][] weight) {
		super();
		this.adjacencyMatrix = adjacencyMatrix;
		this.weight = weight;
	}

	/**
	 * Adding a edge between two vertices into the graph.
	 * 
	 * @param firstVertex
	 *            the first of two vertices.
	 * @param secondVertex
	 *            the second of two vertices.
	 * @param numberOfEdges
	 *            the number of edges to add between two vertices.
	 * @return boolean
	 */
	public abstract void addEdge(int firstVertex, int secondVertex, int numberOfEdges);

	public abstract void addEdgeWeight(int firstVertex, int secondVertex, double weight);

	/**
	 * Adding vertices into the graph
	 * 
	 * @param addNumber
	 *            the number of vertices to add into the graph
	 */
	public void addVertices(int addNumber) {
		int numberOfMatrix = this.adjacencyMatrix.length;
		int[][] newMatrix = new int[numberOfMatrix + addNumber][numberOfMatrix + addNumber];
		for (int i = 0; i < numberOfMatrix; i++) {
			for (int j = 0; j < numberOfMatrix; j++) {
				newMatrix[i][j] = this.adjacencyMatrix[i][j];
			}
		}
		setAdjacencyMatrix(newMatrix);
	}

	/**
	 * Using a search algorithms to traverse a Graph
	 * 
	 * @param searchAlgorithm
	 *            the search method is used for searching operation
	 */
	public abstract List<Integer> breadthFirstSearch(Searchable searchAlgorithm);

	@Override
	public int[][] clone() {
		int numberOfVertices = this.adjacencyMatrix.length;
		int[][] copyOfAdjacencyMatrix = new int[numberOfVertices][numberOfVertices];
		for (int i = 0; i < numberOfVertices; i++) {
			copyOfAdjacencyMatrix[i] = this.getAdjacencyMatrix()[i].clone();
		}
		return copyOfAdjacencyMatrix;
	}

	/**
	 * Delete a edge between two vertices
	 * 
	 * @param firstVerices
	 *            the beginning vertex
	 * @param secondVerices
	 *            the ending vertex
	 * @return void
	 */
	public abstract void deleteEdge(int firstVertex, int secondVertex);

	/**
	 * Using a search algorithms to traverse a Graph
	 * 
	 * @param searchAlgorithm
	 *            the search method is used for searching operation
	 */
	public abstract List<Integer> depthFirstSearch(Searchable searchAlgorithm);

	/**
	 * Find a feasible Hamilton circuits of the graph.
	 * 
	 * @param algorithm
	 *            The algorithm is used for finding Hamilton circuit.
	 * @return a Queue contain all graph's vertices ordered by Hamilton circuit
	 */
	public Queue<Integer> findAHamiltonCircuits(HamiltonianCircuitSearchable algorithm) {
		return algorithm.searchHamiltonCircuit(this);
	}

	/**
	 * Find all feasible Hamilton circuits of the graph.
	 * 
	 * @param algorithm
	 *            The algorithm is used for finding Hamilton circuit.
	 * 
	 * @return a Queue contain all graph's vertices ordered by Hamilton circuit
	 */
	public List<Queue<Integer>> findAllHamiltonCircuits(HamiltonianCircuitSearchable algorithm) {
		return algorithm.searchHamiltonCircuitByBruteForce(this);
	}

	/**
	 * Find Eulerian circuit of this graph
	 * 
	 * @param connectedCheckingAlgorithm
	 *            the checking algorithm to use
	 * @return a Queue contain all graph's vertices ordered by Eulerian circuit
	 */
	public Queue<Integer> findEulerianCircuit(ConnectedGraphCheckable connectedCheckingAlgorithm,
			EulerianPathSearchable eulerianPathSearchAlgorithm) {
		// Check is Euler or not
		if (!this.isEuler(connectedCheckingAlgorithm))
			return null;
		return eulerianPathSearchAlgorithm.findEulerianCircuit(this);
	}

	/**
	 * Find Eulerian trail of this graph
	 * 
	 * @param connectedCheckingAlgorithm
	 *            the connected checking algorithm to be used
	 * @return a Queue contain all graph's vertices ordered by Eulerian trail
	 */
	public Queue<Integer> findEulerianTrail(ConnectedGraphCheckable connectedCheckingAlgorithm,
			EulerianPathSearchable eulerianPathSearchAlgorithm) {
		// All of steps like findEulerianCircuit
		int[] u = new int[1];
		if (!this.isHalfEuler(u, connectedCheckingAlgorithm))
			return null;
		return eulerianPathSearchAlgorithm.findEulerianTrail(this, u[0]);
	}

	/**
	 * Using Dijkstra algorithm to find a shortest path a vertex to other vertex.
	 * 
	 * @param start
	 *            the starting vertex
	 * @param end
	 *            the destination vertex
	 * @param dijkstra
	 *            Dijkstra object
	 * @return A Queue contain a path from starting vertex to destination vertex
	 */
	public abstract Queue<Integer> findShortestPathUsingDijkstra(int start, int end, Dijkstra dijkstra);

	/**
	 * Using Ployd algorithm to find a shortest path a vertex to other vertex.
	 * 
	 * @param start
	 *            the starting vertex
	 * @param end
	 *            the destination vertex
	 * @param ployd
	 *            Dijkstra object
	 * @return A Queue contain a path from starting vertex to destination vertex
	 */
	public abstract Queue<Integer> findShortestPathUsingPloyd(int start, int end, Ployd ployd);

	/**
	 * Find a way from a vertex to another vertex
	 * 
	 * @param beginningVertex
	 *            the beginning vertex
	 * @param endingVertex
	 *            the ending vertex
	 * @param connectedCheckingAlgorithm
	 *            Algorithm is used to find way
	 * @return A queue contain all vertices in order of the way from A to B
	 */
	public Queue<Integer> findWayBetweenTwoConnectedVertices(int firstVertex, int secondVertex,
			ConnectedGraphCheckable connectedCheckingAlgorithm) {
		return connectedCheckingAlgorithm.findWayBetweenTwoConnectedVertices(firstVertex, secondVertex, this);
	}

	/**
	 * @return the adjacencyMatrix
	 */
	public int[][] getAdjacencyMatrix() {
		return adjacencyMatrix;
	}

	/**
	 * Using a search algorithms to traverse a Graph, then collect all of graph's
	 * connected components. When it has finish collecting, it return the collection
	 * 
	 * @param searchAlgorithm
	 *            the search method is used for searching operation
	 * @return the list of connected components
	 */
	public abstract List<List<Integer>> getConnectedComponents(Searchable searchAlgorithm);

	/**
	 * This method is used for get degree of a vertices.
	 * 
	 * @param vertexIndex
	 *            the vertices to get the degree.
	 * @return degree of a vertices.
	 */
	public abstract int getDegree(int vertexIndex);

	/**
	 * Using a search algorithms to traverse a Graph, then count the number of
	 * connected vertices
	 * 
	 * @param searchAlgorithm
	 *            the search method is used for searching operation.
	 * 
	 * @return the number of connected vertices
	 */
	public abstract int getNumberOfConnectedComponents(Searchable searchAlgorithm);

	/**
	 * Getting the number of edges
	 * 
	 * @return the number of edges
	 */
	public abstract int getNumberOfEdges();

	/**
	 * @return the weight
	 */
	public double[][] getWeight() {
		return weight;
	}

	/**
	 * Assign Infinitive value to all value of weight matrix
	 * 
	 * @param negative
	 *            dedicate Infinitive value are negative or positive
	 */
	public void initInfinitive(boolean negative) {
		if (negative) {
			Arrays.fill(this.weight, Double.NEGATIVE_INFINITY);
		} else {
			Arrays.fill(this.weight, Double.POSITIVE_INFINITY);
		}
	}

	/**
	 * Check whether the graph is a complete graph (Kn) or not.
	 * 
	 * @param connectedCheckingAlgorithm
	 *            connectedCheckingAlgorithm the connected checking algorithm to be
	 *            used.
	 * @return true if and only if the graph is complete, false otherwise.
	 */
	public boolean isComplete(ConnectedGraphCheckable connectedCheckingAlgorithm) {
		if (!this.isConnected(connectedCheckingAlgorithm))
			return false;

		int numberOfVertices = this.adjacencyMatrix.length;
		int standartDegree = numberOfVertices - 1;
		for (int[] is : adjacencyMatrix) {
			for (int i : is) {
				if (this.getDegree(i) != standartDegree)
					return false;
			}
		}
		return true;
	}

	/**
	 * @return true if and only if the graph is connected; false if the graph is
	 *         disconnected
	 */
	public abstract boolean isConnected(ConnectedGraphCheckable connectedCheckingAlgorithm);

	/**
	 * Check whether two vertices are connected with each other.
	 * 
	 * @param firstVertex
	 *            the beginning vertex
	 * @param secondVertex
	 *            the ending vertex
	 * @param connectedCheckingAlgorithm
	 *            the checking algorithm to use
	 * @return true if and only if the graph is connected; false if the graph is
	 *         disconnected
	 */
	public abstract boolean isConnected(int firstVertex, int secondVertex,
			ConnectedGraphCheckable connectedCheckingAlgorithm);

	/**
	 * Check whether the graph is a Euler graph or not
	 * 
	 * @param connectedCheckingAlgorithm
	 *            the checking algorithm to use
	 * @return true if the graph is a Euler graph, false otherwise
	 */
	public abstract boolean isEuler(ConnectedGraphCheckable connectedCheckingAlgorithm);

	/** Overloading */
	public abstract boolean isHalfEuler(ConnectedGraphCheckable connectedCheckingAlgorithm);

	/**
	 * Check whether the graph is a half Euler graph
	 * 
	 * @param getBeginningVertex
	 *            the array to store a beginning vertex for finding Eulerian trail
	 *            operation at 0 index
	 * @return true if the graph is a half Euler graph, false otherwise
	 */
	public abstract boolean isHalfEuler(int[] getBeginningVertex, ConnectedGraphCheckable connectedCheckingAlgorithm);

	/**
	 * Using JFileChooser to choose a file then load adjacency matrix from that file
	 * 
	 * @throws IOException
	 * @throws NumberFormatException
	 */
	public void loadAdjacencyMatrix() throws NumberFormatException, IOException {
		File file = UsingFileChooser.getFileFromJFileChooser(false);
		if (file != null) {
			loadAdjacencyMatrix(file);
		}
	}

	/**
	 * Load adjacency matrix from a file and assign to current object's adjacency
	 * matrix
	 * 
	 * @param inputFile
	 *            the file to be read
	 * @throws IOException
	 * @throws NumberFormatException
	 */
	public void loadAdjacencyMatrix(File inputFile) throws NumberFormatException, IOException {
		if (!inputFile.exists())
			return;
		BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)));
		int numberOfVertices = Integer.parseInt(input.readLine());

		int[][] matrix = new int[numberOfVertices][numberOfVertices];

		for (int i = 0; i < numberOfVertices; i++) {
			String[] tempArray = input.readLine().split(" ");
			for (int j = 0; j < numberOfVertices; j++) {
				matrix[i][j] = Integer.parseInt(tempArray[j]);

			}
		}
		this.adjacencyMatrix = matrix;
		input.close();
	}

	/**
	 * Using JFileChooser to choose a file then load weight matrix from that file
	 * 
	 * @param negative
	 *            dedicate Infinitive value will negative or positive
	 * @throws IOException
	 * @throws NumberFormatException
	 */
	public void loadWeightMatrix(boolean negative) throws NumberFormatException, IOException {
		File file = UsingFileChooser.getFileFromJFileChooser(false);
		if (file != null) {
			loadWeightMatrix(file);
		}
	}

	/**
	 * Load weight matrix from a file and assign to current object's weight matrix
	 * 
	 * @param inputFile
	 *            the file to be read
	 * @param negative
	 *            dedicate Infinitive value will negative or positive
	 * @throws IOException
	 * @throws NumberFormatException
	 */
	public void loadWeightMatrix(File inputFile) throws NumberFormatException, IOException {
		if (!inputFile.exists())
			return;
		BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)));
		int numberOfVertices = Integer.parseInt(input.readLine());
		double[][] weight = new double[numberOfVertices][numberOfVertices];

		for (int i = 0; i < numberOfVertices; i++) {
			String[] tempArray = input.readLine().split(" ");
			for (int j = 0; j < numberOfVertices; j++) {
				try {
					weight[i][j] = Double.parseDouble(tempArray[j]);
				} catch (NumberFormatException e) {
					if ("-".equalsIgnoreCase(tempArray[j])) {
						weight[i][j] = Double.NEGATIVE_INFINITY;
					} else {
						weight[i][j] = Double.POSITIVE_INFINITY;
					}
				}
			}
		}
		input.close();
		this.weight = weight;
		this.adjacencyMatrix = createAdjacencyMatrixFromWeight(weight);
	}

	public final int numberOfVertice() {
		return this.adjacencyMatrix.length;
	}

	/**
	 * Printing a matrix to console.
	 */
	public void printMatrix() {
		System.out.println(this.toString());
	}

	/**
	 * Using JFileChooser to choose a file then save adjacency matrix into that file
	 * 
	 * @throws FileNotFoundException
	 */
	public void saveAdjacencyMatrix() throws FileNotFoundException {
		File file = UsingFileChooser.getFileFromJFileChooser(true);
		if (file != null) {
			saveAdjacencyMatrix(file);
		}
	}

	/**
	 * Save adjacency matrix into a file
	 * 
	 * @param outputFile
	 * @throws FileNotFoundException
	 */
	public void saveAdjacencyMatrix(File outputFile) throws FileNotFoundException {
		PrintWriter output = new PrintWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));

		output.println(this.adjacencyMatrix.length);

		for (int i = 0; i < adjacencyMatrix.length; i++) {
			StringBuilder line = new StringBuilder();
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				line.append(this.adjacencyMatrix[i][j] + " ");
			}
			output.println(line.toString().trim());
		}
		output.close();
	}

	/**
	 * Using JFileChooser to choose a file then save weight matrix into that file
	 * 
	 * @throws FileNotFoundException
	 */
	public void saveWeightMatrix() throws FileNotFoundException {
		File file = UsingFileChooser.getFileFromJFileChooser(true);
		if (file != null) {
			saveWeightMatrix(file);
		}
	}

	/**
	 * Save weight matrix into a file
	 * 
	 * @param outFile
	 * @throws FileNotFoundException
	 */
	public void saveWeightMatrix(File outputFile) throws FileNotFoundException {
		PrintWriter output = new PrintWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));
		output.println(this.weight.length);

		for (int i = 0; i < adjacencyMatrix.length; i++) {
			StringBuilder line = new StringBuilder();
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				double temp = this.weight[i][j];

				if (Double.NEGATIVE_INFINITY == temp)
					line.append("- ");
				else if (Double.POSITIVE_INFINITY == temp)
					line.append("+ ");
				else
					line.append(temp + " ");

			}
			output.println(line.toString().trim());
		}
		output.close();
	}

	/**
	 * @param adjacencyMatrix
	 *            the adjacencyMatrix to set
	 */
	public void setAdjacencyMatrix(int[][] adjacencyMatrix) {
		this.adjacencyMatrix = adjacencyMatrix;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(double[][] weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		StringBuilder tempString = new StringBuilder();

		if (this.adjacencyMatrix != null) {
			tempString.append("Adjacency Matrix:\n");
			tempString.append(GraphUils.integerArrayToString(this.adjacencyMatrix));
		}

		if (this.weight != null) {
			tempString.append("Weight Matrix:\n");
			tempString.append(GraphUils.doubleArrayToString(this.weight));
		}

		return tempString.toString();
	}

	// public static void main(String[] args) {
	// Graph graph = new UndirectedGraph();
	// graph.loadAdjacencyMatrix(new
	// File("B:\\baotruong11a\\TestFolder\\Graph_Test_Case\\test_euler_un.txt"));
	// System.out.println(
	// graph.findEulerianCircuit(new DFSCheckingConnectedGraph(), new
	// EulerianPathUsingHierholzerAlgorithm()));
	// System.out.println(
	// graph.findEulerianTrail(new DFSCheckingConnectedGraph(), new
	// EulerianPathUsingHierholzerAlgorithm()));
	// graph.printMatrix();
	// }

}
