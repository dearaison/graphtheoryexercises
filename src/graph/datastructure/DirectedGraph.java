package graph.datastructure;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import graph.connectedgraph.ConnectedGraphCheckable;
import graph.searchalgorithms.Searchable;
import graph.shortestpathfinding.Dijkstra;
import graph.shortestpathfinding.Ployd;

/**
 * This class is subclass of Graph. This class also consist of directed graph's
 * method which is not defined in Graph.
 */
public class DirectedGraph extends Graph {

	/**
	 * 
	 */
	public DirectedGraph() {
		super();
	}

	/**
	 * @param weight
	 */
	public DirectedGraph(double[][] weight) {
		super(weight);
	}

	/**
	 * @param adjacencyMatrix
	 */
	public DirectedGraph(int[][] adjacencyMatrix) {
		super(adjacencyMatrix);
	}

	/**
	 * @param adjacencyMatrix
	 * @param weight
	 */
	public DirectedGraph(int[][] adjacencyMatrix, double[][] weight) {
		super(adjacencyMatrix, weight);
	}

	@Override
	public void addEdge(int beginningVertex, int endingVertex, int numberOfEdges) {
		this.adjacencyMatrix[beginningVertex][endingVertex] += numberOfEdges;
	}

	@Override
	public void addEdgeWeight(int firstVertex, int secondVertex, double weight) {
		this.weight[firstVertex][secondVertex] = weight;
	}

	@Override
	public List<Integer> breadthFirstSearch(Searchable searchAlgorithm) {
		// convert the directed graph to undirected graph
		Graph undirectedGraph = this.converToUndirectedGraph();
		// the number of vertices
		int numberOfVertices = this.adjacencyMatrix.length;
		int beginningVertex = 0;

		// List of visited vertices
		ArrayList<Integer> visitedList = new ArrayList<Integer>();

		// This code is to handle disconnected graph
		while (visitedList.size() < numberOfVertices) {
			// if the beginning vertex is visited, we won't traverse from it.

			// That because when a vertex was visited once time, then all
			// vertices
			// which connect with that vertex also were visited
			if (!visitedList.contains(beginningVertex))
				searchAlgorithm.breadthFirstSearch(beginningVertex, undirectedGraph, visitedList);
			beginningVertex++;
		}
		System.out.println(visitedList.toString());
		return visitedList;
	}

	/**
	 * Convert a directed graph to a undirected graph
	 * 
	 * @return a undirected graph
	 */
	public UndirectedGraph converToUndirectedGraph() {
		int numberOfVertices = this.adjacencyMatrix.length;
		int[][] undirectedAdjacencyMatrix = new int[numberOfVertices][numberOfVertices];
		for (int i = 0; i < numberOfVertices; i++) {
			for (int j = 0; j < numberOfVertices; j++) {
				if (undirectedAdjacencyMatrix[i][j] < this.adjacencyMatrix[i][j]) {
					undirectedAdjacencyMatrix[i][j] = undirectedAdjacencyMatrix[j][i] = this.adjacencyMatrix[i][j];
				}
			}
		}
		double[][] undirectedWeightMatrix = new double[numberOfVertices][numberOfVertices];
		if (this.weight != null) {
			for (int i = 0; i < numberOfVertices; i++) {
				for (int j = 0; j < numberOfVertices; j++) {
					if (Double.NEGATIVE_INFINITY < this.weight[i][j] && this.weight[i][j] < Double.POSITIVE_INFINITY) {
						undirectedWeightMatrix[i][j] = undirectedWeightMatrix[j][i] = this.weight[i][j];
					} else {
						undirectedWeightMatrix[i][j] = this.weight[i][j];
					}
				}
			}
		}
		return new UndirectedGraph(undirectedAdjacencyMatrix, undirectedWeightMatrix);
	}

	@Override
	public void deleteEdge(int beginnigVertex, int secondVertex) {
		this.adjacencyMatrix[beginnigVertex][secondVertex]--;
	}

	@Override
	public List<Integer> depthFirstSearch(Searchable searchAlgorithm) {
		// convert the directed graph to undirected graph
		Graph undirectedGraph = this.converToUndirectedGraph();
		// the number of vertices
		int numberOfVertices = this.adjacencyMatrix.length;
		int beginningVertex = 0;

		// List of visited vertices
		ArrayList<Integer> visitedList = new ArrayList<Integer>();

		// This code is to handle disconnected graph
		while (visitedList.size() < numberOfVertices) {
			// if the beginning vertex is visited, we won't traverse from it.

			// That because when a vertex was visited once time, then all
			// vertices
			// which connect with that vertex also were visited
			if (!visitedList.contains(beginningVertex))
				searchAlgorithm.depthFirstSearch(beginningVertex, undirectedGraph, visitedList);
			beginningVertex++;
		}
		System.out.println(visitedList.toString());
		return visitedList;
	}

	@Override
	public Queue<Integer> findShortestPathUsingDijkstra(int start, int end, Dijkstra dijkstra) {
		return dijkstra.getShortestPath(start, end);
	}

	@Override
	public Queue<Integer> findShortestPathUsingPloyd(int start, int end, Ployd ployd) {
		return ployd.getShortestPath(start, end);
	}

	@Override
	public List<List<Integer>> getConnectedComponents(Searchable searchAlgorithm) {
		// convert the directed graph to undirected graph
		Graph undirectedGraph = this.converToUndirectedGraph();

		// the method result
		List<List<Integer>> connectedComponentsList = new ArrayList<>();
		// this list temporarily store all of vertices of a connected component
		ArrayList<Integer> aConnectedComponent;
		// List of visited vertices
		ArrayList<Integer> visitedList = new ArrayList<Integer>();

		// the number of vertices
		int numberOfVertices = this.adjacencyMatrix.length;
		int beginningVertex = 0;

		// This code is to handle disconnected graph
		while (visitedList.size() < numberOfVertices) {
			// if the beginning vertex is visited, we won't traverse from it.

			// That because when a vertex was visited once time, then all
			// vertices
			// which connect with that vertex also were visited
			if (!visitedList.contains(beginningVertex)) {
				searchAlgorithm.depthFirstSearch(beginningVertex, undirectedGraph, visitedList);
				aConnectedComponent = new ArrayList<>();
				// Traversing from the first vertex to the last vertex of
				// current connected
				// component
				for (int i = visitedList.indexOf(beginningVertex); i < visitedList.size(); i++) {
					// Add all vertices of current connected components into
					// aConnectedComponent
					aConnectedComponent.add(visitedList.get(i));
				}
				// Add the connected component into result list
				connectedComponentsList.add(aConnectedComponent);
			}
			beginningVertex++;
		}
		System.out.println(connectedComponentsList.toString());
		return connectedComponentsList;
	}

	@Override
	public int getDegree(int vertexIndex) {
		return getOutDegree(vertexIndex) + getInDegree(vertexIndex);
	}

	/**
	 * This method is used for getting incoming degree of a vertices.
	 * 
	 * @param vertexIndex
	 *            the vertices to get the degree.
	 * @return incoming degree of a vertices.
	 */
	public int getInDegree(int vertexIndex) {
		int inDegree = 0;
		int numberOfVertices = this.adjacencyMatrix.length;
		for (int i = 0; i < numberOfVertices; i++) {
			inDegree += this.adjacencyMatrix[i][vertexIndex];
		}
		return inDegree;
	}

	@Override
	public int getNumberOfConnectedComponents(Searchable searchAlgorithm) {
		// convert the directed graph to undirected graph
		Graph undirectedGraph = this.converToUndirectedGraph();
		// the result variable
		int result = 0;
		// the number of vertices
		int numberOfVertices = this.adjacencyMatrix.length;
		int beginningVertex = 0;

		// List of visited vertices
		ArrayList<Integer> visitedList = new ArrayList<Integer>();

		// This code is to handle disconnected graph
		while (visitedList.size() < numberOfVertices) {
			// if the beginning vertex is visited, we won't traverse from it.

			// That because when a vertex was visited once time, then all
			// vertices
			// which connect with that vertex also were visited
			if (!visitedList.contains(beginningVertex)) {
				searchAlgorithm.depthFirstSearch(beginningVertex, undirectedGraph, visitedList);
				result++;
			}
			beginningVertex++;
		}
		return result;
	}

	@Override
	public int getNumberOfEdges() {
		int numberOfEdges = 0;
		int numberOfVertices = this.adjacencyMatrix.length;
		for (int i = 0; i < numberOfVertices; i++) {
			for (int j = 0; j < numberOfVertices; j++) {
				numberOfEdges += this.adjacencyMatrix[i][j];
			}
		}
		return numberOfEdges;
	}

	/**
	 * getting outgoing degree of a vertices.
	 * 
	 * @param vertexIndex
	 *            the vertices to get the degree.
	 * @return outgoing degree of a vertices.
	 */
	public int getOutDegree(int vertexIndex) {
		int outDegree = 0;
		int numberOfVertices = this.adjacencyMatrix.length;
		for (int j = 0; j < numberOfVertices; j++) {
			outDegree += this.adjacencyMatrix[vertexIndex][j];
		}
		return outDegree;
	}

	@Override
	public boolean isConnected(ConnectedGraphCheckable connectedCheckingAlgorithm) {
		return isWeakConnected(connectedCheckingAlgorithm);
	}

	@Override
	public boolean isConnected(int firstVertex, int secondVertex, ConnectedGraphCheckable connectedCheckingAlgorithm) {
		return connectedCheckingAlgorithm.isTwoVerticesConnected(firstVertex, secondVertex, this);
	}

	@Override
	public boolean isEuler(ConnectedGraphCheckable connectedCheckingAlgorithm) {
		// if the graph is not connected, it won't be a Euler graph
		if (!this.isConnected(connectedCheckingAlgorithm)) {
			return false;
		}
		int numberOfVertices = this.adjacencyMatrix.length;
		for (int i = 0; i < numberOfVertices; i++) {
			// if there is only one vertex
			// which has out going degree greater than incoming degree
			// or out going degree less than in coming degree,
			// it won't be balance. it won't has Eulerian circuits
			if (this.getInDegree(i) != this.getOutDegree(i))
				return false;
		}
		return true;
	}

	@Override
	public boolean isHalfEuler(ConnectedGraphCheckable connectedCheckingAlgorithm) {
		// if the graph is not connected, it won't has Eulerian trails
		if (!this.isConnected(connectedCheckingAlgorithm)) {
			return false;
		}
		int numberOfVertices = this.adjacencyMatrix.length;

		// the vertex which has out going degree greater than in coming degree
		// by one
		int outGreaterIn = -1;
		// the vertex which has in coming degree greater than out going degree
		// by one
		int inGreaterOut = -1;

		// list of balanced vertices
		ArrayList<Integer> balanceVertices = new ArrayList<Integer>();

		for (int i = 0; i < numberOfVertices; i++) {
			int tempInDegree = this.getInDegree(i);
			int tempOutDegree = this.getOutDegree(i);
			if (tempOutDegree == tempInDegree + 1) {
				outGreaterIn = i;
			} else if (tempInDegree == tempOutDegree + 1) {
				inGreaterOut = i;
			} else if (tempOutDegree == tempInDegree) {
				balanceVertices.add(i);
			}
		}

		// the graph will be half Euler graph,
		// if and only if the graph is Euler graph or
		// there are only two vertices which satisfy conditions:
		// -degree(x) = +degree(x) + 1
		// +degree(y) = -degree(y) + 1
		// and all other vertices are balance
		if (balanceVertices.size() == numberOfVertices
				|| outGreaterIn > -1 && inGreaterOut > -1 && balanceVertices.size() == numberOfVertices - 2) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isHalfEuler(int[] outGreaterThanInVertex, ConnectedGraphCheckable connectedCheckingAlgorithm) {
		// if the graph is not connected, it won't has Eulerian trails
		if (!this.isConnected(connectedCheckingAlgorithm)) {
			return false;
		}
		int numberOfVertices = this.adjacencyMatrix.length;

		// the vertex which has out going degree greater than in coming degree
		// by one
		int outGreaterIn = -1;
		// the vertex which has in coming degree greater than out going degree
		// by one
		int inGreaterOut = -1;

		// number of balanced vertices
		int numberOfBalanced = 0;

		for (int i = 0; i < numberOfVertices; i++) {
			int tempInDegree = this.getInDegree(i);
			int tempOutDegree = this.getOutDegree(i);
			if (tempOutDegree == tempInDegree + 1) {
				outGreaterIn = i;
			} else if (tempInDegree == tempOutDegree + 1) {
				inGreaterOut = i;
			} else if (tempOutDegree == tempInDegree) {
				numberOfBalanced++;
			}
		}

		// the graph will be half Euler graph,
		// if and only if the graph is Euler graph or
		// there are only two vertices which satisfy conditions:
		// +degree(y) = -degree(y) + 1
		// -degree(x) = +degree(x) + 1
		// and all other vertices are balance
		if (numberOfBalanced == numberOfVertices
				|| outGreaterIn > -1 && inGreaterOut > -1 && numberOfBalanced == numberOfVertices - 2) {
			outGreaterThanInVertex[0] = outGreaterIn;
			return true;
		} else {
			return false;
		}
	}

	public boolean isStrongConnected(ConnectedGraphCheckable connectedCheckingAlgorithm) {
		int numberOfVertices = this.adjacencyMatrix.length;
		for (int i = 0; i < numberOfVertices; i++) {
			for (int j = 0; j < numberOfVertices; j++) {
				if (i != j && !connectedCheckingAlgorithm.isTwoVerticesConnected(i, j, this)) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean isWeakConnected(ConnectedGraphCheckable connectedCheckingAlgorithm) {
		return this.converToUndirectedGraph().isConnected(connectedCheckingAlgorithm);
	}
}
