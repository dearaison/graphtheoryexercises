package graph.datastructure;

import java.util.Collection;

/**
 * Created by Eclipse on 23 January, 2018 8:11:15 PM.
 *
 * @author Joseph Maria
 *
 */
public class GraphUils {
	/**
	 * Convert a two dimension array to a string.
	 * 
	 * @param array
	 *            array used for converting
	 * @return a string of the argument array
	 */
	public static final String doubleArrayToString(double[][] array) {
		StringBuilder tempString = new StringBuilder();
		for (double[] row : array) {
			for (double aElement : row) {
				if (Double.isFinite(aElement)) {
					tempString.append(aElement + "\t");
				} else {
					if (Double.compare(aElement, Double.POSITIVE_INFINITY) == 0)
						tempString.append("oo");
					else
						tempString.append("-oo");
				}
			}
			tempString.append("\n");
		}
		return tempString.toString();
	}

	/**
	 * Convert a two dimension array to a string.
	 * 
	 * @param array
	 *            array used for converting
	 * @return a string of the argument array
	 */
	public static final String integerArrayToString(int[][] array) {
		StringBuilder tempString = new StringBuilder();
		for (int[] row : array) {
			for (int aElement : row) {
				tempString.append(aElement + "\t");
			}
			tempString.append("\n");
		}
		return tempString.toString();
	}

	/**
	 * Convert a path in a collection to a String.
	 * 
	 * @return a String of the path on format: "Vertex->Vertex".
	 */
	public static final String pathToString(Collection<Integer> path) {
		StringBuilder tempString = new StringBuilder();
		for (Integer vertex : path) {
			tempString.append(vertex + "->");
		}
		return tempString.delete(tempString.lastIndexOf("-"), tempString.length()).toString();
	}

	/**
	 * Printing a path in a collection to console.
	 */
	public static final void printPath(Collection<Integer> path) {
		System.out.println(pathToString(path));
	}

}
