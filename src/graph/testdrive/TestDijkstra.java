package graph.testdrive;

import java.io.IOException;

import org.junit.Test;

import exception.NegativeEdgeWeightException;
import graph.datastructure.UndirectedGraph;
import graph.shortestpathfinding.Dijkstra;

public class TestDijkstra {

	@Test
	public void test() throws NegativeEdgeWeightException, NumberFormatException, IOException {
		UndirectedGraph test = new UndirectedGraph();
		test.loadWeightMatrix(false);

		Dijkstra dijkstra = new Dijkstra(0, test);
		System.out.println(dijkstra.getShortestPath(0, 2));
		System.out.println(dijkstra.getShortestPath(0, 1, 2));
	}
}
