package graph.testdrive;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import graph.bipartilegraph.BipartiteGraphCheckable;
import graph.bipartilegraph.CheckingBipartiteGraphNonRecursive;
import graph.datastructure.UndirectedGraph;

/**
 * Created by Eclipse on 23 January, 2018 4:45:42 PM.
 *
 * @author Joseph Maria
 *
 */
public class TestBipartite {

	@Test
	public void test() throws NumberFormatException, IOException {
		int G[][] = { { 0, 1, 0, 1 }, { 1, 0, 1, 0 }, { 0, 1, 0, 1 }, { 1, 0, 1, 0 } };
		UndirectedGraph bipartiteGraph = new UndirectedGraph(G);

		int G2[][] = { { 0, 1, 0, 0, 1, 1 }, { 1, 0, 1, 1, 1, 1 }, { 0, 1, 0, 1, 0, 1 }, { 0, 1, 1, 0, 1, 0 },
				{ 1, 1, 0, 1, 0, 1 }, { 1, 1, 1, 0, 1, 0 } };
		UndirectedGraph nonBipartiteGraph = new UndirectedGraph(G2);

		int[][] G3 = { { 0, 1, 1, 0, 0, 0 }, { 1, 0, 0, 1, 0, 0 }, { 0, 0, 1, 0, 0, 0 }, { 0, 0, 1, 1, 0, 0 },
				{ 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 1 } };
		UndirectedGraph nonBipartiteGraph2 = new UndirectedGraph(G3);

		BipartiteGraphCheckable bipartiteCheckingAlgorithm = new CheckingBipartiteGraphNonRecursive();

		assertTrue(bipartiteGraph.isBipartite(bipartiteCheckingAlgorithm));
		assertFalse(nonBipartiteGraph.isBipartite(bipartiteCheckingAlgorithm));
		assertFalse(nonBipartiteGraph2.isBipartite(bipartiteCheckingAlgorithm));
	}

}
