package graph.testdrive;

import org.junit.Before;
import org.junit.Test;

import graph.datastructure.DirectedGraph;
import graph.datastructure.GraphUils;
import graph.shortestpathfinding.Ployd;

public class TestPloyd {
	DirectedGraph directedGraph;
	int numberOfVertices;

	@Before
	public void initTest() {
		directedGraph = new DirectedGraph(new double[6][6]);

		directedGraph.initInfinitive(false);
		numberOfVertices = directedGraph.getWeight().length;

		// directedGraph.addEdgeWeight(0, 2, 5);
		// directedGraph.addEdgeWeight(0, 1, 7);
		// directedGraph.addEdgeWeight(1, 2, 7);
		// directedGraph.addEdgeWeight(1, 3, 6);
		// directedGraph.addEdgeWeight(3, 1, 1);
		// directedGraph.addEdgeWeight(3, 2, 11);
		// directedGraph.addEdgeWeight(3, 0, 4);

		directedGraph.addEdgeWeight(0, 1, 7);
		directedGraph.addEdgeWeight(0, 3, 2);
		directedGraph.addEdgeWeight(1, 2, 4);
		directedGraph.addEdgeWeight(1, 4, 1);
		directedGraph.addEdgeWeight(2, 5, 3);
		directedGraph.addEdgeWeight(3, 1, 4);
		directedGraph.addEdgeWeight(4, 0, 2);
		directedGraph.addEdgeWeight(4, 2, 2);
		directedGraph.addEdgeWeight(5, 1, 1);
	}

	@Test
	public void test() {
		Ployd ployd = new Ployd(directedGraph);
		double[][] w = ployd.getWeight1();
		int[][] p = ployd.getPreviousVertex1();

		StringBuilder tempString = new StringBuilder();

		tempString.append("Length: \n");
		tempString.append(GraphUils.doubleArrayToString(w));
		tempString.append("Previous vertex: \n");
		tempString.append(GraphUils.integerArrayToString(p));

		System.out.println(tempString.toString());
		GraphUils.printPath(ployd.getShortestPath(1, 5));
	}

}
