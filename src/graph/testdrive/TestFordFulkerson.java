package graph.testdrive;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import graph.datastructure.DirectedGraph;
import graph.maxflow.FordFulkersonAlgorithm;
import graph.maxflow.MaxFlowInterface;

/**
 * Created by Eclipse on 25 January, 2018 12:32:41 AM.
 *
 * @author Joseph Maria
 *
 */
public class TestFordFulkerson {

	@Test
	public void test() {
		double[][] graph = { { 0, 16, 13, 0, 0, 0 }, { 0, 0, 10, 12, 0, 0 }, { 0, 4, 0, 0, 14, 0 },
				{ 0, 0, 9, 0, 0, 20 }, { 0, 0, 0, 7, 0, 4 }, { 0, 0, 0, 0, 0, 0 } };

		double[][] predictableFlow = { { 0.0, -12.0, -11.0, 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0, 12.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 11.0, 0.0 }, { 0.0, 0.0, 0.0, 0.0, 0.0, 19.0 }, { 0.0, 0.0, 0.0, 7.0, 0.0, 4.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 } };

		DirectedGraph directedGraph = new DirectedGraph(graph);

		MaxFlowInterface maxFlow = new FordFulkersonAlgorithm(directedGraph, 0, 5);

		assertArrayEquals(predictableFlow, maxFlow.getFlow());
		assertEquals(23, maxFlow.getMaxFlowValue());

		System.out.println(maxFlow.getMinCut());
	}

}
