package graph.testdrive;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import graph.bipartilegraph.MaximumBipartiteMatching;
import graph.datastructure.UndirectedGraph;

/**
 * Created by Eclipse on 25 January, 2018 1:17:40 PM.
 *
 * @author Joseph Maria
 *
 */
public class TestMaximumBipartiteMatching {

	/**
	 * Test method for
	 * {@link graph.bipartilegraph.MaximumBipartiteMatching#countTheMaximumMaching(graph.datastructure.UndirectedGraph)}.
	 */
	@Test
	public void testCountTheMaximumMaching() {
		int G[][] = { { 0, 1, 0, 1 }, { 1, 0, 1, 0 }, { 0, 1, 0, 1 }, { 1, 0, 1, 0 } };

		UndirectedGraph graph = new UndirectedGraph(G);
		assertEquals(4, MaximumBipartiteMatching.countTheMaximumMaching(graph));
	}

}
