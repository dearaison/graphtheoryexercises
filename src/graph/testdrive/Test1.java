package graph.testdrive;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Queue;

import org.junit.*;

import graph.bipartilegraph.BipartiteGraphCheckable;
import graph.bipartilegraph.CheckingBipartiteGraphNonRecursive;
import graph.connectedgraph.ConnectedGraphCheckable;
import graph.connectedgraph.DFSCheckingConnectedGraph;
import graph.datastructure.DirectedGraph;
import graph.datastructure.Graph;
import graph.datastructure.UndirectedGraph;
import graph.hamiltoncircuitalgorithms.HamiltonBacktracking;
import graph.hamiltoncircuitalgorithms.HamiltonianCircuitSearchable;
import graph.searchalgorithms.DFSAndBFSNonRecursive;
import graph.searchalgorithms.Searchable;

public class Test1 {
	UndirectedGraph graph1, graph3, graph4, graph5, graph6;
	DirectedGraph graph2;
	BipartiteGraphCheckable bipartiteGraphAlgorithm;
	ConnectedGraphCheckable connectedheckingAlgorithm;
	Searchable searchAlgorithm;

	// @Rule
	// public Timeout gobalTimeOut = new Timeout(2, TimeUnit.MILLISECONDS);

	@Before
	public void initializeVariable() {
		bipartiteGraphAlgorithm = new CheckingBipartiteGraphNonRecursive();
		connectedheckingAlgorithm = new DFSCheckingConnectedGraph();
		searchAlgorithm = new DFSAndBFSNonRecursive();
		int[][] G = { { 0, 1, 0, 1 }, { 1, 0, 1, 0 }, { 0, 1, 0, 1 }, { 1, 0, 1, 0 } };
		int[][] G2 = { { 0, 1, 0, 1 }, { 0, 0, 1, 0 }, { 1, 0, 0, 0 }, { 0, 0, 0, 0 } };
		int[][] G3 = { { 0, 1, 0, 1 }, { 1, 0, 1, 0 }, { 0, 1, 0, 1 }, { 1, 0, 1, 0 } };
		int[][] G4 = { { 0, 1, 1, 0, 0, 0, 1 }, { 1, 0, 1, 1, 0, 1, 0 }, { 1, 1, 0, 1, 0, 0, 1 },
				{ 0, 1, 1, 0, 0, 1, 1 }, { 0, 0, 0, 0, 0, 0, 1 }, { 0, 1, 0, 1, 0, 0, 0 }, { 1, 0, 1, 1, 1, 0, 0 } };
		int G5[][] = { { 0, 1, 1, 0, 0, 0, 0 }, { 0, 0, 0, 1, 0, 0, 0 }, { 0, 1, 0, 1, 1, 0, 0 },
				{ 0, 0, 0, 0, 1, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0 }, };
		int G6[][] = { { 0, 1, 1, 1, 0, 0 }, { 1, 0, 1, 0, 1, 0 }, { 1, 1, 0, 1, 0, 0 }, { 1, 0, 1, 0, 1, 1 },
				{ 0, 1, 0, 0, 0, 1 }, { 0, 0, 0, 0, 1, 0 } };

		graph1 = new UndirectedGraph(G);
		graph2 = new DirectedGraph(G2);
		graph3 = new UndirectedGraph(G3);
		graph4 = new UndirectedGraph(G4);
		graph5 = new UndirectedGraph(G5);
		graph6 = new UndirectedGraph(G6);
	}

	@Test
	public void testFindWayBetweenTwoConnectedVertices() {
		System.out.println("way: " + graph6.findWayBetweenTwoConnectedVertices(0, 5, connectedheckingAlgorithm));
	}

	@Test
	public void testGetNumberOfConnectedComponents() {
		assertEquals(graph1.getNumberOfConnectedComponents(searchAlgorithm), 1);
		assertEquals(graph2.getNumberOfConnectedComponents(searchAlgorithm), 1);
		assertEquals(graph3.getNumberOfConnectedComponents(searchAlgorithm), 1);
		assertEquals(graph4.getNumberOfConnectedComponents(searchAlgorithm), 1);
		assertEquals(graph5.getNumberOfConnectedComponents(searchAlgorithm), 3);
	}

	@Test
	public void testHamiltonCircuitSearchingMethod() {
		int[][] matrix = { { 0, 1, 1, 0, 0, 1 }, { 1, 0, 1, 0, 0, 1 }, { 1, 1, 0, 1, 1, 0 }, { 0, 0, 1, 0, 1, 1 },
				{ 0, 0, 1, 1, 0, 1 }, { 1, 1, 0, 1, 1, 0 } };
		Graph graph = new UndirectedGraph(matrix);
		HamiltonianCircuitSearchable algorithm = new HamiltonBacktracking();
		List<Queue<Integer>> lists = graph.findAllHamiltonCircuits(algorithm);
		System.out.println("Hamilton Circuits: ");
		for (Queue<Integer> list : lists) {
			System.out.println(list);
		}
	}

	@Test
	public void testIsBipartite() {
		assertTrue(graph1.isBipartite(bipartiteGraphAlgorithm));
		assertTrue(graph3.isBipartite(bipartiteGraphAlgorithm));
		assertFalse(graph4.isBipartite(bipartiteGraphAlgorithm));
	}

	@Test
	public void testSearch() {
		System.out.println("depthFirstSearch");

		graph1.depthFirstSearch(searchAlgorithm);
		System.out.println("================================");

		graph2.depthFirstSearch(searchAlgorithm);
		System.out.println("================================");

		graph3.depthFirstSearch(searchAlgorithm);
		System.out.println("================================");

		graph4.depthFirstSearch(searchAlgorithm);
		System.out.println("================================");

		graph5.depthFirstSearch(searchAlgorithm);
		System.out.println("================================");
	}

	@Test
	public void testStrongConnected() {
		assertFalse(graph2.isStrongConnected(connectedheckingAlgorithm));
		assertTrue(graph2.isConnected(connectedheckingAlgorithm));
		assertTrue(graph2.isConnected(2, 1, connectedheckingAlgorithm));
		assertFalse(graph2.isConnected(3, 2, connectedheckingAlgorithm));
	}

	@Test
	public void testUndirectedConnected() {
		assertTrue(graph1.isConnected(connectedheckingAlgorithm));
		assertTrue(graph1.isConnected(1, 3, connectedheckingAlgorithm));
	}
}
