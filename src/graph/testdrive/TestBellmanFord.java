package graph.testdrive;

import org.junit.Test;

import graph.datastructure.Graph;
import graph.datastructure.UndirectedGraph;
import graph.shortestpathfinding.BellmanFord;

/**
 * Created by Eclipse
 * 
 * @author Joseph Maria Date Jan 18, 2018 Time 12:36:17 AM
 */
public class TestBellmanFord {

	@Test
	public void test() {
		double[][] weight = { { 1, 2, 0 }, { 1, 2, 0 }, { 1, 2, 3 } };
		Graph test = new UndirectedGraph(weight);

		BellmanFord bellmanFord = new BellmanFord(0, test);
		System.out.println(bellmanFord.getShortestPath(0, 2));
		System.out.println(bellmanFord.getShortestPath(0, 1, 2));
	}

}
