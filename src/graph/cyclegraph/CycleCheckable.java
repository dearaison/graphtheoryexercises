package graph.cyclegraph;

import graph.datastructure.Graph;

/**
 * This interface consist of algorithms to check whether a given graph is cycle
 * or not.
 */
public interface CycleCheckable {
	/**
	 * Check whether the graph has a cycle or not
	 * 
	 * @param graph
	 *            the graph to check
	 * @return true if and only if there is a cycle, false otherwise
	 */
	public boolean isCycle(Graph graph, boolean[] vistedVertices);
}
