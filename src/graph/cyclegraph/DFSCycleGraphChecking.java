package graph.cyclegraph;

import graph.datastructure.Graph;

public class DFSCycleGraphChecking implements CycleCheckable {
	private int[][] adjacencyMatrix;

	/**
	 * A recursive function that uses visited[] and parent to detect cycle in
	 * subgraph reachable from vertex currentVertex.
	 * 
	 * @param currentVertex
	 * @param parentVertex
	 * @param visitedVertices
	 * @return true if and only if there is a cycle, false otherwise
	 */
	private boolean checkCycleUtil(int currentVertex, int parentVertex, boolean[] visitedVertices) {
		// Mark the current vertex as visited
		visitedVertices[currentVertex] = true;

		// Recur for all the vertices adjacent to this vertex
		for (int v = 0; v < this.adjacencyMatrix.length; v++) {
			if (adjacencyMatrix[currentVertex][v] > 0) {
				// If an adjacent is not visited, then recur for
				// that adjacent
				if (!visitedVertices[v]) {
					if (checkCycleUtil(v, currentVertex, visitedVertices)) {
						return true;
					}
				}
				// If an adjacent is visited and not parent of
				// current vertex, then there is a cycle.
				else if (v != parentVertex) {
					return true;
				}
			}
		}
		// If we don't find such an adjacent for any vertex,
		// we say that there is no cycle
		return false;
	}

	@Override
	public boolean isCycle(Graph graph, boolean[] visitedVertices) {
		this.adjacencyMatrix = graph.getAdjacencyMatrix();
		return checkCycleUtil(0, -1, visitedVertices);
	}

}
