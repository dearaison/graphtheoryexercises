package graph.eulerianpathalgorithms;

import java.util.Queue;

import graph.datastructure.Graph;

/**
 * This interface consist of algorithms to find Eulerian circuit and Eulerian
 * trail
 */
public interface EulerianPathSearchable {
	/**
	 * This method search and return a Eulerian circuit from a given Eulerian graph
	 * 
	 * @param graph
	 *            the given Eulerian graph
	 * @return the list contain all vertices of given graph in order of Eulerian
	 *         circuit
	 */
	public Queue<Integer> findEulerianCircuit(Graph graph);

	/**
	 * This method search and return a Eulerian trail from a given half Eulerian or
	 * Eulerian graph
	 * 
	 * @param graph
	 *            the given half Eulerian or Eulerian graph
	 * @param beginningVertex
	 *            the beginning vertex to start search operation; a Odd vertex for
	 *            undirected graph and a unbalanced vertex for directed graph
	 * @return the list contain all vertices of given graph in order of Eulerian
	 *         trail
	 */
	public Queue<Integer> findEulerianTrail(Graph graph, int beginningVertex);
}
