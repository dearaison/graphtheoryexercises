package graph.eulerianpathalgorithms;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import graph.datastructure.Graph;

public class EulerianPathUsingHierholzerAlgorithm implements EulerianPathSearchable {

	@Override
	public Queue<Integer> findEulerianCircuit(Graph graph) {
		return findEulerianTrail(graph, 0);
	}

	@Override
	public Queue<Integer> findEulerianTrail(Graph graph, int beginningVertex) {
		// copy the graph, because we will remove all edges in graph
		int[][] copyOfAdjacencyMatrix = graph.clone();

		// current standing vertex
		int u = beginningVertex;

		// get the number of edges
		int numberOfEdge = graph.getNumberOfEdges();

		// get the number of vertices
		int numberOfVertices = graph.getAdjacencyMatrix().length;

		// the adjacency matrix of the graph
		int[][] adjacencyMatrix = graph.getAdjacencyMatrix();

		// the list contain the path of circuit
		LinkedList<Integer> pathList = new LinkedList<Integer>();
		pathList.add(u);

		// It is not until there is no edge that the loop stops.
		while (numberOfEdge > 0) {
			// the list of sub path
			List<Integer> tempPath = new ArrayList<>();
			// the variable stores the index which candidate where sub path
			// should be insert into path list
			int insertIndex = -1;
			for (int i = 0; i < pathList.size(); i++) {
				// with each vertex in pathList, if there is at least one vertex
				// which still has at least one edge, we will traverse sub path
				// form that vertex

				// Note that: 'i' is position of vertex in path list, and value in path list at
				// i
				int tempVertex = pathList.get(i);
				if (graph.getDegree(tempVertex) > 0) {
					insertIndex = i;
					// add into sub path and remove from path list the vertex which is the beginning
					// of sub path to
					// prevent duplicate vertex
					tempPath.add(pathList.remove(i));
					// jump into sub path beginning vertex
					u = tempVertex;
					// go out the for loop
					break;
				}
			}
			// while current standing vertex still has at least one degree,
			// then find the way to next vertex
			// which is an adjacent vertex of the current standing vertex
			while (graph.getDegree(u) > 0) {
				// find way
				for (int v = 0; v < numberOfVertices; v++) {
					if (adjacencyMatrix[u][v] > 0) {
						// add a next adjacent vertex of current standing vertex in to sub path
						// delete the edge between current standing vertex and the next vertex
						// decrease number of vertices
						// jump into next adjacent vertex
						tempPath.add(v);
						graph.deleteEdge(u, v);
						numberOfEdge--;
						u = v;
						break;
					}
				}
			}
			pathList.addAll(insertIndex, tempPath);
		}
		// restore adjacency matrix of the graph
		graph.setAdjacencyMatrix(copyOfAdjacencyMatrix);
		return pathList;
	}

}
