package graph.searchalgorithms;

import java.util.Collection;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import graph.datastructure.Graph;

/**
 * This class implement Searchable interface. The class implement BFS and DFS
 * algorithms using recursion to accomplish traversing the graph.
 * 
 * Note that this procedure is not easy to understand with few beginner, but
 * source code is shorten
 */
public class DFSAndBFSRecursive implements Searchable {

	private static void breathSearch(Queue<Integer> queue, Graph graph, Collection<Integer> visitedList) {
		if (queue.isEmpty())
			return;
		// Poll a vertex from queue as current vertex for searching operation
		int u = queue.poll();
		// Get adjacency matrix of current vertex
		int[] adjacencyMatrix = graph.getAdjacencyMatrix()[u];
		// Search in current vertex's adjacency matrix
		// to find all adjacent vertices which is adjacent to current vertex.
		for (int v = 0; v < adjacencyMatrix.length; v++) {
			// if found a adjacent vertex of current vertex,
			// we will mark it was visited and add it into the queue
			if (adjacencyMatrix[v] > 0 && !visitedList.contains(v)) {
				visitedList.add(v);
				queue.add(v);
			}
		}
		breathSearch(queue, graph, visitedList);
	}

	@Override
	public void breadthFirstSearch(int currentVertex, Graph graph, Collection<Integer> visitedList) {
		// Create a queue (FIFO) of vertex numbers and
		// enqueue source vertex for BFS traversal
		ConcurrentLinkedQueue<Integer> queue = new ConcurrentLinkedQueue<Integer>();
		// Before recursion, we add in to visited list the first current vertex as the
		// beginning vertex
		visitedList.add(currentVertex);
		// Add in to queue the first vertex
		queue.add(currentVertex);
		// Call recursive method to start recursion operation
		breathSearch(queue, graph, visitedList);
	}

	@Override
	public void depthFirstSearch(int currentVertex, Graph graph, Collection<Integer> visitedList) {
		// Add current vertex into visited list
		visitedList.add(currentVertex);
		// Get adjacency matrix of current vertex
		int[] adjacencyMatrix = graph.getAdjacencyMatrix()[currentVertex];
		// Search in current vertex's adjacency matrix
		// to find adjacent vertices which is adjacent to current vertex.
		for (int v = 0; v < adjacencyMatrix.length; v++) {
			// if found a adjacent vertex of current vertex, we will continue searching
			// operation
			// from that adjacent vertex
			if (adjacencyMatrix[v] > 0 && !visitedList.contains(v)) {
				depthFirstSearch(v, graph, visitedList);
			}
		}
	}

}
