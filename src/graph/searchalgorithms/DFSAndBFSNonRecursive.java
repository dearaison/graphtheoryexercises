package graph.searchalgorithms;

import java.util.Collection;
import java.util.Stack;
import java.util.concurrent.ConcurrentLinkedQueue;

import graph.datastructure.Graph;

/**
 * This class implement Searchable interface. The class implement BFS and DFS
 * algorithms using data structures to accomplish traversing the graph
 */
public class DFSAndBFSNonRecursive implements Searchable {

	public void breadthFirstSearch(int beginningVertex, Graph graph, Collection<Integer> visitedList) {
		// number of vertices
		int numberOfVertices = graph.getAdjacencyMatrix().length;

		// graph's adjacency matrix
		int[][] adjacencyMatrix = graph.getAdjacencyMatrix();

		// add beginning vertex into visited vertices's list
		visitedList.add(beginningVertex);

		// Create a stack (FIFO) of vertex numbers and
		// enqueue source vertex for BFS traversal
		ConcurrentLinkedQueue<Integer> verticesQueue = new ConcurrentLinkedQueue<Integer>();

		// push the beginning vertex into queue
		verticesQueue.add(beginningVertex);

		// u is current vertex
		int u;

		// boolean variable to dedicate when to stop the loop.
		// the loop must be stopped when all of vertices were visited and added into
		// visited list (visitedList.size() == size) or
		// the vertices queue is empty
		while (visitedList.size() < numberOfVertices && !verticesQueue.isEmpty()) {
			u = verticesQueue.poll();
			for (int v = 0; v < numberOfVertices; v++) {
				// An edge from u to v exists and
				// destination v is not visited and not in visited list
				if (adjacencyMatrix[u][v] > 0 && !visitedList.contains(v)) {
					// push v into queue
					// add v into visited list
					verticesQueue.add(v);
					visitedList.add(v);
				}
			}
		}
	}

	public void depthFirstSearch(int beginningVertex, Graph graph, Collection<Integer> visitedList) {
		// number of vertices
		int numberOfVertices = graph.getAdjacencyMatrix().length;

		// graph's adjacency matrix
		int[][] adjacencyMatrix = graph.getAdjacencyMatrix();

		// Create a stack (LIFO) of vertex numbers and
		// enqueue source vertex for DFS traversal
		Stack<Integer> verticesStack = new Stack<>();

		// set u as beginning vertex
		int u = beginningVertex;

		// add beginning vertex into visited list
		visitedList.add(beginningVertex);

		// boolean variable to dedicate when to stop the loop.
		// the loop must be stopped when all of vertices were visited and added into
		// visited list (visitedList.size() == size) or
		// the vertices stack is empty
		boolean stop = false;
		mainLoop: while (visitedList.size() < numberOfVertices && !stop) {
			stop = true;
			for (int v = 0; v < numberOfVertices; v++) {
				// An edge from u to v exists and
				// destination v is not visited and not in visited list
				if (adjacencyMatrix[u][v] > 0 && !visitedList.contains(v)) {
					// add v into visited list
					// push u vertex to stack
					// move to v vertex
					// change stop into false to keep looping
					visitedList.add(v);
					verticesStack.push(u);
					u = v;
					stop = false;
					continue mainLoop;
				}
			}
			// if there is no unvisited vertex which can reach from u vertex, we
			// will go back the previous vertex (get it in vertices stack) to find other
			// way.
			if (stop && !verticesStack.empty()) {
				u = verticesStack.pop();
				stop = false;
			}
		}
	}

}
