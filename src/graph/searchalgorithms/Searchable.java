package graph.searchalgorithms;

import java.util.Collection;

import graph.datastructure.Graph;

/**
 * This interface consists of all method to traverse graph as BFS or DFS
 */
public interface Searchable {
	/**
	 * breadth first search algorithm
	 * 
	 * @param beginningVertex
	 *            the beginning vertex of algorithm
	 * @param graph
	 *            the graph to be search
	 * @param visitedList
	 *            the list of visited vertices
	 */
	public void breadthFirstSearch(int beginningVertex, Graph graph, Collection<Integer> visitedList);

	/**
	 * depth first search algorithm
	 * 
	 * @param beginningVertex
	 *            the beginning vertex of algorithm
	 * @param graph
	 *            the graph to be search
	 * @param visitedList
	 *            the list of visited vertices
	 */
	public void depthFirstSearch(int beginningVertex, Graph graph, Collection<Integer> visitedList);
}
