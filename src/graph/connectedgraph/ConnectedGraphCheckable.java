package graph.connectedgraph;

import java.util.Queue;

import graph.datastructure.Graph;

/**
 * Interface consist all of method are used for checking connection of graph
 */
public interface ConnectedGraphCheckable {
	/**
	 * Count the number of vertices, vertices are in a connected component
	 * 
	 * @param beginningVertex
	 *            the beginning vertex.
	 * @param graph
	 *            The graph to check connected or disconnected
	 * @return the number of vertices in a connected component
	 */
	public int countConnectedVertices(int beginningVertex, Graph graph);

	/**
	 * Find a way from a vertex to another vertex
	 * 
	 * @param beginningVertex
	 *            the beginning vertex
	 * @param endingVertex
	 *            the ending vertex
	 * @param graph
	 *            The graph to find a way
	 * @return A list contain all vertices in order of the way from A to B
	 */
	public Queue<Integer> findWayBetweenTwoConnectedVertices(int beginningVertex, int endingVertex, Graph graph);

	/**
	 * Check whether there is at least one way to go from a vertex to another vertex
	 * 
	 * @param beginningVertex
	 *            the beginning vertex
	 * @param endingVertex
	 *            the ending vertex
	 * @param graph
	 *            The graph to check connected or disconnected
	 * @return true if and only if there is at least one way to go from beginning
	 *         vertex to ending vertex, false otherwise
	 */
	public boolean isTwoVerticesConnected(int beginningVertex, int endingVertex, Graph graph);
}
