package graph.connectedgraph;

import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ConcurrentLinkedDeque;

import graph.datastructure.Graph;

/**
 * This class implement ConnectedGraphCheckable interface. It uses DFS algorithm
 * to check a graph connected or not Note: In this case, DFS faster than BFS.
 */
public class DFSCheckingConnectedGraph implements ConnectedGraphCheckable {

	public int countConnectedVertices(int beginningVertex, Graph graph) {
		int numberOfVertices = graph.getAdjacencyMatrix().length;
		int[][] graphMatrix = graph.getAdjacencyMatrix();
		int u = beginningVertex;

		// array to mark a vertex is visited.
		boolean[] visited = new boolean[numberOfVertices];

		// mark begging vertex as visited one.
		visited[u] = true;
		int count = 1;

		// Create a stack (LIFO) of vertex numbers and
		// enqueue source vertex for DFS traversal
		Stack<Integer> verticesStack = new Stack<>();

		// boolean variable to dedicate when to stop the loop
		boolean stop = false;

		// the loop must be stopped
		// when all of vertices are visited (count == size) or
		// the vertices stack is empty
		mainLoop: while (count < numberOfVertices && !stop) {
			stop = true;
			for (int v = 0; v < numberOfVertices; v++) {
				// An edge from u to v exists and
				// destination v is not visited
				if (graphMatrix[u][v] > 0 && !visited[v]) {
					// push u vertex to stack
					// move to v vertex
					// mark v vertex is visited
					// Increase count by one
					// change stop into false to keep looping
					verticesStack.push(u);

					u = v;
					visited[v] = true;
					count++;
					stop = false;
					continue mainLoop;
				}
			}
			// if there is no unvisited vertex which can reach from u vertex,
			// we will go back the previous vertex (get it in vertices stack) to find other
			// way.
			if (stop && !verticesStack.isEmpty()) {
				u = verticesStack.pop();
				stop = false;
			}
		}
		return count;
	}

	public Queue<Integer> findWayBetweenTwoConnectedVertices(int beginningVertex, int endingVertex, Graph graph) {
		int numberOfVertices = graph.getAdjacencyMatrix().length;
		int[][] graphMatrix = graph.getAdjacencyMatrix();
		int u = beginningVertex;

		// array to mark a vertex is visited.
		boolean[] visited = new boolean[numberOfVertices];

		// mark begging vertex as visited one.
		visited[u] = true;

		// list contain all vertices in order of the way between two vertices
		ConcurrentLinkedDeque<Integer> way = new ConcurrentLinkedDeque<Integer>();

		// Create a stack (LIFO) of vertex numbers and
		// enqueue source vertex for DFS traversal
		Stack<Integer> verticesStack = new Stack<>();

		// boolean variable to dedicate when to stop the loop
		// the loop must be stopped when the vertices stack is empty
		boolean stop = false;
		mainLoop: while (!stop) {
			stop = true;
			// if there is a way from beginning vertex to ending vertex, the
			// loop should be
			// stopped;
			// otherwise continue looping.
			if (visited[endingVertex]) {
				way.add(endingVertex);
				return way;
			}
			for (int v = 0; v < numberOfVertices; v++) {
				// An edge from u to v exists and
				// destination v is not visited
				if (graphMatrix[u][v] > 0 && !visited[v]) {
					// push u vertex to stack
					// move to v vertex
					// mark v vertex is visited
					// change stop into false to keep looping
					verticesStack.push(u);

					// if u is not in way list then add it into way list
					if (!way.contains(u))
						way.addLast(u);

					u = v;
					visited[v] = true;
					stop = false;
					continue mainLoop;
				}
			}
			// if there is no unvisited vertex which can reach from u vertex, we
			// will go back the previous vertex (get it in vertices stack) to
			// find other
			// way.
			if (stop && !verticesStack.isEmpty()) {
				u = verticesStack.pop();
				// remove last vertex because it won't lead to ending vertex
				way.removeLast();
				stop = false;
			}
		}
		return null;
	}

	public boolean isTwoVerticesConnected(int beginningVertex, int endingVertex, Graph graph) {
		int numberOfVertices = graph.getAdjacencyMatrix().length;
		int[][] graphMatrix = graph.getAdjacencyMatrix();
		int u = beginningVertex;

		// array to mark a vertex is visited.
		boolean[] visited = new boolean[numberOfVertices];

		// mark begging vertex as visited one.
		visited[u] = true;

		// Create a stack (LIFO) of vertex numbers and
		// enqueue source vertex for DFS traversal
		Stack<Integer> verticesStack = new Stack<>();

		// boolean variable to dedicate when to stop the loop
		// the loop must be stopped when the vertices stack is empty
		boolean stop = false;
		mainLoop: while (!stop) {
			stop = true;
			// if there is a way from beginning vertex to ending vertex, the
			// loop should be
			// stopped;
			// otherwise continue looping.
			if (visited[endingVertex]) {
				return true;
			}
			for (int v = 0; v < numberOfVertices; v++) {
				// An edge from u to v exists and
				// destination v is not visited
				if (graphMatrix[u][v] > 0 && !visited[v]) {
					// push u vertex to stack
					// move to v vertex
					// mark v vertex is visited
					// change stop into false to keep looping
					verticesStack.push(u);

					u = v;
					visited[v] = true;
					stop = false;
					continue mainLoop;
				}
			}
			// if there is no unvisited vertex which can reach from u vertex,
			// we will go back the previous vertex (get it in vertices stack) to find other
			// way.
			if (stop && !verticesStack.isEmpty()) {
				u = verticesStack.pop();
				stop = false;
			}
		}
		return false;
	}

}
