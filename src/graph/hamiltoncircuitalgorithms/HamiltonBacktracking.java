package graph.hamiltoncircuitalgorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import graph.datastructure.Graph;

public class HamiltonBacktracking implements HamiltonianCircuitSearchable {
	// the matrix to temporarily store graph adjacency matrix
	private int[][] adjacencyMatrix;

	// temporarily store the number of vertices
	private int numberOfVertices;

	// Array to mark vertices are visited or not
	private boolean[] visited;

	// Array temporarily store a Hamiltonian circuit.
	private int[] tempHamiltonianCircuit;

	// List contain one Hamiltonian circuit.
	private Queue<Integer> aHamiltonianCircuit;

	// List contain as many as possible Hamiltonian circuits.
	private List<Queue<Integer>> allOfFeasibleHamiltonCircuit;

	/**
	 * Initialize all members variable before running algorithm
	 * 
	 * @param graph
	 *            The graph need finding Hamiltonian circuit.
	 */
	private void initMenberVariable(Graph graph) {
		this.adjacencyMatrix = graph.getAdjacencyMatrix();
		this.numberOfVertices = graph.getAdjacencyMatrix().length;
		tempHamiltonianCircuit = new int[numberOfVertices];
		visited = new boolean[numberOfVertices];
		this.allOfFeasibleHamiltonCircuit = new ArrayList<>();
		// Let us put vertex 0 as the first vertex in the path. If there is a
		// Hamiltonian Cycle, then the path can be started from any point of the
		// cycle
		// as the graph is undirected
		this.tempHamiltonianCircuit[0] = 0;
		this.visited[0] = true;
	}

	/**
	 * Search recursively the graph to find all feasible Hamiltonian circuits
	 * 
	 * @param currentPosition
	 *            the pointer indicate the index where the next vertex of
	 *            Hamiltonian circuit should be stored
	 */
	private void searchAllFeasiblePath(int currentPosition) {
		// Try different vertices as a next candidate in
		// Hamiltonian Cycle. We don't try for 0 as we
		// included 0 as starting point in initMenberVariable()
		for (int v = 1; v < this.numberOfVertices; v++) {

			// Check if this vertex v can be added to Hamiltonian Cycle:
			// The vertex has not already been included
			// The vertex is an adjacent vertex of the previously added
			if (!visited[v] && this.adjacencyMatrix[this.tempHamiltonianCircuit[currentPosition - 1]][v] > 0) {
				// add next vertex of circuit
				this.tempHamiltonianCircuit[currentPosition] = v;

				// Base case: If all vertices are included in Hamiltonian Cycle
				// If not, recur to construct rest of the path
				if (currentPosition + 1 < numberOfVertices) {
					// set vertex v is visited
					this.visited[v] = true;

					// recur to construct rest of the path
					searchAllFeasiblePath(currentPosition + 1);

					// mark vertex v is not visited; also known as we remove it
					// if adding vertex v
					// doesn't lead to a solution
					this.visited[v] = false;

					// We reached the last vertex of circuit
					// And if there is an edge from the last included
					// vertex to the first vertex
				} else if (this.adjacencyMatrix[this.tempHamiltonianCircuit[currentPosition]][this.tempHamiltonianCircuit[0]] > 0) {
					// Create a list to store one circuit
					this.aHamiltonianCircuit = new ConcurrentLinkedQueue<>();
					// Add vertices in order of Hamiltonian circuit
					for (int i : this.tempHamiltonianCircuit) {
						this.aHamiltonianCircuit.add(i);
					}
					// Let us add the first vertex again to construct the
					// complete cycle
					this.aHamiltonianCircuit.add(this.tempHamiltonianCircuit[0]);
					this.allOfFeasibleHamiltonCircuit.add(aHamiltonianCircuit);
				}
			}
		}
	}

	@Override
	public Queue<Integer> searchHamiltonCircuit(Graph graph) {
		initMenberVariable(graph);
		// If there is a solution, the method will return the list of circuit,
		// otherwise
		// return null
		if (searchOneFeasiblePath(1)) {
			return aHamiltonianCircuit;
		} else {
			return null;
		}
	}

	@Override
	public List<Queue<Integer>> searchHamiltonCircuitByBruteForce(Graph graph) {
		initMenberVariable(graph);
		searchAllFeasiblePath(1);
		return this.allOfFeasibleHamiltonCircuit;
	}

	/**
	 * Search recursively the graph to find at least one feasible Hamiltonian
	 * circuits
	 * 
	 * @param currentPosition
	 *            the pointer indicate the index where the next vertex of
	 *            Hamiltonian circuit should be stored
	 * @return true if there is a solution, false otherwise
	 */
	private boolean searchOneFeasiblePath(int currentPosition) {
		// Base case: If all vertices are included in Hamiltonian Cycle
		if (currentPosition == numberOfVertices) {

			// And if there is an edge from the last included
			// vertex to the first vertex
			if (this.adjacencyMatrix[this.tempHamiltonianCircuit[currentPosition
					- 1]][this.tempHamiltonianCircuit[0]] > 0) {

				// Create a list to store one circuit
				this.aHamiltonianCircuit = new ConcurrentLinkedQueue<Integer>();

				for (int i : this.tempHamiltonianCircuit) {
					this.aHamiltonianCircuit.add(i);
				}

				// Let us add the first vertex again to construct the
				// complete cycle
				this.aHamiltonianCircuit.add(this.tempHamiltonianCircuit[0]);
				return true;
			} else {
				return false;
			}
		}

		// Try different vertices as a next candidate in
		// Hamiltonian Circuit. We don't try for 0 as we
		// included 0 as starting point in in initMenberVariable()
		for (int v = 1; v < this.numberOfVertices; v++) {

			// Check if this vertex v can be added to Hamiltonian Cycle:
			// The vertex has not already been included
			// The vertex is an adjacent vertex of the previously added
			if (!visited[v] && this.adjacencyMatrix[this.tempHamiltonianCircuit[currentPosition - 1]][v] > 0) {
				tempHamiltonianCircuit[currentPosition] = v;
				visited[v] = true;

				// recur to construct rest of the path
				if (searchOneFeasiblePath(currentPosition + 1))
					return true;

				// mark vertex v is not visited; also known as we remove it if
				// adding vertex v
				// doesn't lead to a solution
				visited[v] = false;
			}
		}
		return false;
	}

}
