package graph.hamiltoncircuitalgorithms;

import java.util.List;
import java.util.Queue;

import graph.datastructure.Graph;

/**
 * This interface consist of algorithms to find Hamiltonian circuit
 */
public interface HamiltonianCircuitSearchable {
	/**
	 * This method search and return a feasible Hamiltonian circuit
	 * 
	 * @param graph
	 *            the given graph
	 * @return the list contain a feasible Hamiltonian circuit
	 */
	public Queue<Integer> searchHamiltonCircuit(Graph graph);

	/**
	 * This method search and return all of feasible Hamiltonian circuit
	 * 
	 * @param graph
	 *            the given graph
	 * @return the list contain all feasible Hamiltonian circuit, each circuit is
	 *         stores in a sub list
	 */
	public List<Queue<Integer>> searchHamiltonCircuitByBruteForce(Graph graph);
}
