package graph.shortestpathfinding;

import java.util.LinkedList;
import java.util.Queue;

public class ShorstestPathUtil {
	public static Queue<Integer> getShortestPathFromPrevivousArray(int start, int end, int[] previousVertex) {
		LinkedList<Integer> path = new LinkedList<Integer>();

		// add end vertex to path list
		path.addFirst(end);
		int u = end;

		// add previous vertices until the start vertex is in path list
		while (u != start) {
			u = previousVertex[u];
			path.addFirst(u);
		}
		return path;
	}
}
