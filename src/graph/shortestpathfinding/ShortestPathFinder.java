package graph.shortestpathfinding;

import java.util.Queue;

/**
 * Interface of shortest path finding algorithms
 * 
 * @author Joseph Maria
 *
 */
public interface ShortestPathFinder {

	/**
	 * This method find a shortest path form a vertex to another vertex base on
	 * weight matrix of a graph.
	 * 
	 * @param start
	 *            the vertex where we start
	 * @param end
	 *            the vertex where is destination
	 * @return a shortest path is contained in a queue, if the weight array is
	 *         incompatible with algorithm, the queue will be empty
	 */
	public Queue<Integer> getShortestPath(int start, int end);

	/**
	 * This method find a shortest path form start vertex to middle vertex and from
	 * middle vertex to end vertex base on weight matrix of a graph.
	 * 
	 * @param start
	 *            the vertex where we start
	 * @param middle
	 *            the vertex which we have to go through it before reaching to end
	 *            vertex
	 * @param end
	 *            the vertex where is destination
	 * @return a shortest path is contained in a queue, if the weight array is
	 *         incompatible with algorithm, the queue will be empty
	 */
	Queue<Integer> getShortestPath(int start, int middle, int end);

}
