package graph.shortestpathfinding;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;

import graph.datastructure.DirectedGraph;

public final class Ployd implements ShortestPathFinder {
	private double[][] weight1;
	private double[][] weight2;
	private int[][] previousVertex1;
	private int[][] previousVertex2;
	private int numberOfvertices;

	/**
	 * @param directedGraph
	 */
	public Ployd(DirectedGraph directedGraph) {
		numberOfvertices = directedGraph.getWeight().length;

		weight1 = directedGraph.getWeight();
		weight2 = new double[numberOfvertices][numberOfvertices];

		previousVertex1 = new int[numberOfvertices][numberOfvertices];
		for (int i = 0; i < numberOfvertices; i++) {
			for (int j = 0; j < numberOfvertices; j++) {
				if (Double.isFinite(weight1[i][j])) {
					previousVertex1[i][j] = j;
				} else {
					previousVertex1[i][j] = -1;
				}
			}
		}

		previousVertex2 = new int[numberOfvertices][numberOfvertices];
		build();
	}

	/**
	 * Run Ployd algorithm to build a distance matrix and a sequence matrix.
	 */
	private void build() {
		for (int k = 0; k < numberOfvertices; k++) {
			for (int i = 0; i < numberOfvertices; i++) {
				for (int j = 0; j < numberOfvertices; j++) {
					if (weight1[i][j] > weight1[i][k] + weight1[k][j]) {
						weight2[i][j] = weight1[i][k] + weight1[k][j];

						// I -> P[i][k] -> K -> J
						// P[i][k] it may be K if the directing path from I to K is the shortest path to
						// go from I to K
						previousVertex2[i][j] = previousVertex1[i][k];
					} else {
						weight2[i][j] = weight1[i][j];
						previousVertex2[i][j] = previousVertex1[i][j];
					}
				}
			}
			weight1 = weight2;
			previousVertex1 = previousVertex2;
			weight2 = new double[numberOfvertices][numberOfvertices];
			previousVertex2 = new int[numberOfvertices][numberOfvertices];
		}
	}

	public int[][] getPreviousVertex1() {
		return previousVertex1;
	}

	@Override
	public Queue<Integer> getShortestPath(int start, int end) {
		ConcurrentLinkedDeque<Integer> path = new ConcurrentLinkedDeque<>();
		int u = start;
		path.add(u);
		while (u != end) {
			u = previousVertex1[u][end];
			path.add(u);
		}
		return path;
	}

	@Override
	public Queue<Integer> getShortestPath(int start, int middle, int end) {
		ConcurrentLinkedQueue<Integer> path = new ConcurrentLinkedQueue<Integer>();
		path.addAll(getShortestPath(start, middle));

		// remove middle vertex in second child path because it has been added into path
		// list
		Queue<Integer> temp = getShortestPath(middle, end);
		temp.poll();
		path.addAll(temp);
		return path;
	}

	@Deprecated
	public Queue<Integer> getShortestPathV2(int start, int end) {
		LinkedList<Integer> path = new LinkedList<Integer>();
		path.add(start);
		while (!path.contains(end)) {
			path.add(previousVertex1[path.getLast()][end]);
		}
		return path;
	}

	public double[][] getWeight1() {
		return weight1;
	}
}
