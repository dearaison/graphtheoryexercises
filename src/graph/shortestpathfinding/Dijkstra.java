package graph.shortestpathfinding;

import java.util.LinkedList;
import java.util.Queue;

import exception.NegativeEdgeWeightException;
import graph.connectedgraph.DFSCheckingConnectedGraph;
import graph.datastructure.UndirectedGraph;

public final class Dijkstra implements ShortestPathFinder {
	private boolean connected;
	private double[][] weight;
	private int numberOfVertices;

	private boolean[] visited;
	private double[] distance;
	private int[] previousVertex;

	/**
	 * Construct a class to get ready to run Dijkstra algorithm.
	 * 
	 * @param start
	 *            start vertex
	 * @param graph
	 * @throws NegativeEdgeWeightException
	 */
	public Dijkstra(int start, UndirectedGraph graph) throws NegativeEdgeWeightException {
		connected = graph.isConnected(new DFSCheckingConnectedGraph());
		weight = graph.getWeight();
		numberOfVertices = graph.getWeight().length;

		for (int i = 0; i < numberOfVertices; i++) {
			for (int j = 0; j < numberOfVertices; j++) {
				if (weight[i][j] < 0)
					throw new NegativeEdgeWeightException(weight[i][j], i, j);
			}
		}
	}

	/**
	 * @return the distance
	 */
	public double[] getDistance() {
		return distance;
	}

	/**
	 * Find the a vertex which have shortest distance from start vertex.
	 * 
	 * @return a vertex from remain list which has minimum distance
	 */
	private int getMinimunDistanceVertex() {
		// Initialize the vertex having minimum distance with the first vertex of remain
		// list
		int minWeightVertex = 0;

		// Initialize minimum distance with positive infinitive
		double minWeight = Double.POSITIVE_INFINITY;

		// for each vertex in remain list find the minimum distance vertex
		for (int v = 0; v < numberOfVertices; v++) {
			if (!visited[v] && distance[v] < minWeight) {
				minWeight = distance[v];
				minWeightVertex = v;
			}

		}
		return minWeightVertex;
	}

	/**
	 * @return the previousVertex
	 */
	public int[] getPreviousVertex() {
		return previousVertex;
	}

	@Override
	public Queue<Integer> getShortestPath(int start, int end) {
		if (!connected) {
			return new LinkedList<Integer>();
		}
		// run algorithm to build previous vertex matrix
		runDijkstra(start, end);

		return ShorstestPathUtil.getShortestPathFromPrevivousArray(start, end, previousVertex);
	}

	@Override
	public Queue<Integer> getShortestPath(int start, int middle, int end) {
		LinkedList<Integer> path = new LinkedList<Integer>();
		path.addAll(getShortestPath(start, middle));

		// remove middle vertex in second child path because it has been added into path
		// list
		Queue<Integer> temp = getShortestPath(middle, end);
		temp.poll();
		path.addAll(temp);
		return path;
	}

	/**
	 * Initialize all member variable to prepare for running algorithm
	 * 
	 * @param start
	 */
	private void init(int start) {
		// Initialize a list contain all of vertices which haven't yet visited
		visited = new boolean[numberOfVertices];

		// Initialize a array contain distance from start vertex to current
		// vertex
		distance = new double[numberOfVertices];
		for (int i = 0; i < numberOfVertices; i++) {
			distance[i] = Double.POSITIVE_INFINITY;
		}
		distance[start] = 0;

		// Initialize a array contain previous vertices of current vertex
		previousVertex = new int[numberOfVertices];
		previousVertex[start] = -1;
	}

	/**
	 * Build previous vertex matrix and length matrix from start vertex to all
	 * vertices in graph.
	 * 
	 * @param start
	 *            start vertex
	 */
	@Deprecated
	public void runDijkstra(int start) {
		if (!connected) {
			return;
		}
		int count = numberOfVertices;
		init(start);
		while (count > 1) {
			int minWeightVertex = getMinimunDistanceVertex();
			visited[minWeightVertex] = true;
			count--;
			for (int v = 0; v < numberOfVertices; v++) {
				if (!visited[v]) {
					double newDistance = distance[minWeightVertex] + weight[minWeightVertex][v];
					if (newDistance < distance[v]) {
						distance[v] = newDistance;
						previousVertex[v] = minWeightVertex;
					}
				}
			}
		}
	}

	/**
	 * Build previous vertex matrix and length matrix from start vertex to end
	 * vertex in graph.
	 * 
	 * @param start
	 * @param end
	 */
	public void runDijkstra(int start, int end) {
		if (!connected) {
			return;
		}

		init(start);

		// set firstly minimum vertex is start vertex
		int minWeightVertex = start;
		while (minWeightVertex != end) {
			// find the minimum vertex
			minWeightVertex = getMinimunDistanceVertex();
			visited[minWeightVertex] = true;
			System.out.println(minWeightVertex);
			for (int v = 0; v < numberOfVertices; v++) {
				if (!visited[v]) {
					// calculate distance of the path form start vertex to v vertex going through
					// minimum vertex
					double distanceFromMinToV = distance[minWeightVertex] + weight[minWeightVertex][v];

					// if distance of the path from start vertex to v vertex going through minimum
					// vertex less than current distance, then reassign previous vertex and distance
					// of v vertex with minimum vertex and new distance.
					if (distanceFromMinToV < distance[v]) {
						distance[v] = distanceFromMinToV;
						previousVertex[v] = minWeightVertex;
					}
				}
			}
		}
	}

}
