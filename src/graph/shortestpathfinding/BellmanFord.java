package graph.shortestpathfinding;

import java.util.LinkedList;
import java.util.Queue;

import graph.datastructure.Graph;

public class BellmanFord implements ShortestPathFinder {
	private double[][] weight;
	private int numberOfVertices;

	private double[] distance;
	private int[] previousVertex;

	/**
	 * Construct a class to get ready to run Dijkstra algorithm.
	 * 
	 * @param start
	 *            start vertex
	 * @param graph
	 *            graph to find path
	 */
	public BellmanFord(int start, Graph graph) {
		weight = graph.getWeight();
		numberOfVertices = graph.getWeight().length;
	}

	/**
	 * @return the distance
	 */
	public double[] getDistance() {
		return distance;
	}

	/**
	 * @return the previousVertex
	 */
	public int[] getPreviousVertex() {
		return previousVertex;
	}

	@Override
	public Queue<Integer> getShortestPath(int start, int end) {
		runBellmanFord(start);

		return ShorstestPathUtil.getShortestPathFromPrevivousArray(start, end, previousVertex);
	}

	@Override
	public Queue<Integer> getShortestPath(int start, int middle, int end) {
		LinkedList<Integer> path = new LinkedList<Integer>();
		path.addAll(getShortestPath(start, middle));

		// remove middle vertex in second child path because it has been added into path
		// list
		Queue<Integer> temp = getShortestPath(middle, end);
		temp.poll();
		path.addAll(temp);
		return path;
	}

	/**
	 * Initialize all member variable to prepare for running algorithm
	 * 
	 * @param start
	 */
	private void init(int start) {
		// Initialize a array contain distance from start vertex to current
		// vertex
		distance = new double[numberOfVertices];
		for (int i = 0; i < numberOfVertices; i++) {
			distance[i] = Double.POSITIVE_INFINITY;
		}
		distance[start] = 0;

		// Initialize a array contain previous vertices of current vertex
		previousVertex = new int[numberOfVertices];
		previousVertex[start] = -1;
	}

	/**
	 * Build previous vertex matrix and length matrix from start vertex to all
	 * vertices in graph.
	 * 
	 * @param start
	 *            start vertex
	 */
	public void runBellmanFord(int start) {
		// count how many loop have been executed
		int count = 0;

		init(start);

		// control when to stop the loop
		boolean stop = false;
		while (!stop) {
			stop = true;
			count++;

			for (int u = 0; u < numberOfVertices; u++) {
				for (int v = 0; v < numberOfVertices; v++) {
					double distanceFromUToV = distance[u] + weight[u][v];
					if (distanceFromUToV < distance[v]) {
						distance[v] = distanceFromUToV;
						previousVertex[v] = u;

						stop = false;
					}
				}
			}

			if (count > numberOfVertices) {
				System.out.println("Negative cycle ERROR!!!");
				break;
			}

		}
	}

}
