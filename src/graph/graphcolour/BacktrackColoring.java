package graph.graphcolour;

import java.util.ArrayList;
import java.util.List;

import graph.datastructure.Graph;

public class BacktrackColoring implements GraphColoring {
	private int[][] adjacencyMatrix;
	private int numberOfVertices;
	private int[] colors;
	private List<int[]> colorArrays;

	/**
	 * A backtracking method to assign colors to vertices.
	 * 
	 * @param currentVertex
	 */
	private void color(int currentVertex) {
		for (int color = 1; color < numberOfVertices; color++) {
			if (ifSafe(currentVertex, color)) {
				colors[currentVertex] = color;
				if (currentVertex + 1 < numberOfVertices) {
					color(currentVertex + 1);
				} else {
					colorArrays.add(colors);
					createNewColorArray();
				}
			}
		}
	}

	@Override
	public int[] colorGraph(Graph graph) {
		return colorGraphWithBruteForce(graph).get(0);
	}

	@Override
	public List<int[]> colorGraphWithBruteForce(Graph graph) {
		init(graph);
		color(1);
		return colorArrays;
	}

	private void createNewColorArray() {
		colors = new int[numberOfVertices];
	}

	/**
	 * Check whether all adjacent vertices of current vertex are assigned by
	 * different color.
	 * 
	 * @param vertex
	 *            current vertex
	 * @param color
	 *            color will be assigned to current vertex
	 * @return true if all adjacent vertices of current vertex are assigned by
	 *         different color, false otherwise.
	 */
	private boolean ifSafe(int vertex, int color) {
		for (int v = 0; v < adjacencyMatrix.length; v++) {
			if (adjacencyMatrix[vertex][v] > 0 && this.colors[v] == color) {
				return false;
			}
		}
		return true;
	}

	/**
	 * initialize all member variable needing for algorithm.
	 * 
	 * @param graph
	 */
	private void init(Graph graph) {
		adjacencyMatrix = graph.getAdjacencyMatrix();
		numberOfVertices = adjacencyMatrix.length;
		createNewColorArray();
		colorArrays = new ArrayList<int[]>();
	}

}
