package graph.graphcolour;

import java.util.List;

import graph.datastructure.Graph;

/**
 * @author Joseph Maria
 *
 */
public interface GraphColoring {

	/**
	 * Color all vertices of graph with a set of color.
	 * 
	 * @param graph
	 *            graph to color
	 * @return a array contain color assigned to each vertex
	 */
	public int[] colorGraph(Graph graph);

	/**
	 * Color all vertices of graph with all feasible set of color.
	 * 
	 * @param graph
	 *            graph to color
	 * @return list contain all of color array
	 */
	public List<int[]> colorGraphWithBruteForce(Graph graph);
}
