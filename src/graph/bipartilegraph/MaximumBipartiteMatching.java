package graph.bipartilegraph;

import java.util.Arrays;

import graph.datastructure.UndirectedGraph;

/*Problem: Maximum Bipartite Matching
 * Real world example:
 * There are M job applicants and N jobs. Each applicant has a subset of jobs
 * that he/she is interested in. Each job opening can only accept one applicant
 * and a job applicant can be appointed for only one job. Find an assignment of
 * jobs to applicants in such that as many applicants as possible get jobs.
 */

/**
 * Created by Eclipse on 25 January, 2018 12:17:51 PM.
 *
 * @author Joseph Maria
 *
 */
public class MaximumBipartiteMatching {

	/**
	 * Count the number of applicants who found job.
	 * 
	 * @param graph
	 * @return the maximum applicants who found job
	 */
	public static int countTheMaximumMaching(UndirectedGraph graph) throws IllegalArgumentException {
		if (!graph.isBipartite(new CheckingBipartiteGraphNonRecursive())) {
			throw new IllegalArgumentException("Argument is not a bipartite graph.");
		}

		int[][] adjacencyMatrix = graph.getAdjacencyMatrix();

		// count the number of applicants who found job.
		int result = 0;

		// An array to keep track of the applicants assigned to
		// jobs. The value of assigned[i] is the applicant number
		// assigned to job i, the value -1 indicates nobody is
		// assigned.
		int[] assigned = new int[adjacencyMatrix.length];
		Arrays.fill(assigned, -1);

		for (int applicant = 0; applicant < assigned.length; applicant++) {

			// Mark all jobs as not seen for next applicant.
			boolean seen[] = new boolean[adjacencyMatrix.length];
			if (findJob(applicant, assigned, seen, adjacencyMatrix)) {
				result++;
			}
		}
		return result;
	}

	/**
	 * A DFS based recursive function that returns true if a matching for vertex u
	 * is possible
	 * 
	 * @param applicant
	 *            the applicant who need a job
	 * @param assigned
	 *            An array to keep track of the applicants assigned to jobs. The
	 *            value of assigned[i] candidate which job was assigned to an
	 *            applicant
	 * @param seen
	 *            Marked the job as current applicant have been applied to
	 * @param adjacencyMatrix
	 * @return True if the applicant found a job, false otherwise
	 */
	private static boolean findJob(int applicant, int[] assigned, boolean[] seen, int[][] adjacencyMatrix) {

		// Try every job one by one
		for (int v = 0; v < adjacencyMatrix.length; v++) {
			// If applicant u is interested in job v and v
			// is not visited
			if (adjacencyMatrix[applicant][v] > 0 && !seen[v]) {
				seen[v] = true; // Mark v as visited

				// If job 'v' is not assigned to an applicant OR
				// previously assigned applicant for job v (which
				// is matchR[v]) has an alternate job available.
				// Since v is marked as visited in the above line,
				// matchR[v] in the following recursive call will
				// not get job 'v' again
				if (assigned[v] < 0 || findJob(assigned[v], assigned, seen, adjacencyMatrix)) {
					assigned[v] = applicant;
					return true;
				}
			}
		}
		return false;
	}
}
