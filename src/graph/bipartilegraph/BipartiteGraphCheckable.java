package graph.bipartilegraph;

import graph.datastructure.Graph;

/**
 * The interface provides all method for checking whether a given graph is
 * bipartite or not
 */
public interface BipartiteGraphCheckable {
	/**
	 * Check whether a given graph is bipartite or not
	 * 
	 * @param startVertices
	 *            The starting vertices
	 * @param graphToCheck
	 *            The graph to check whether Bipartite or not
	 * @return true if graph G[U][V] is Bipartite, else false
	 */
	public boolean isBipartite(int startVertices, Graph graphToCheck);

	/**
	 * Check whether a given graph is bipartite or not
	 * 
	 * @param startVertices
	 *            The starting vertices
	 * @param graphToCheck
	 *            The graph to check whether Bipartite or not
	 * @param colorArray
	 *            The array contains two color which are used to mark the graph
	 *            vertices
	 * @return true if graph G[U][V] is Bipartite, else false
	 */
	public boolean isBipartite(int startVertices, Graph graphToCheck, int[] colorArray);
}
