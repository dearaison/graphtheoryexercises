package graph.bipartilegraph;

import java.util.concurrent.ConcurrentLinkedQueue;

import graph.datastructure.Graph;

/**
 * This class implement BipartiteGraphCheckable interface and define a method to
 * check bipartite graph using BFS.
 * 
 * Note: BFS is faster than DFS in this case.
 */
public class CheckingBipartiteGraphNonRecursive implements BipartiteGraphCheckable {

	@Override
	public boolean isBipartite(int startVertices, Graph graphToCheck) {
		return isBipartite(startVertices, graphToCheck, new int[graphToCheck.getAdjacencyMatrix().length]);
	}

	@Override
	public boolean isBipartite(int startVertices, Graph graphToCheck, int[] colorArray) {
		// the number of vertices
		int numberOfVertices = graphToCheck.getAdjacencyMatrix().length;

		// The color array to store colors assigned
		// to all vertices. Vertex number is used as
		// index in this array. The value '0' of
		// colorArr[i] is used to indicate that no color
		// is assigned to vertex 'i'. The value '-1' is used
		// to indicate first color is assigned and value
		// '1' indicates second color is assigned.
		colorArray[startVertices] = 1;

		int[][] adjacencyMatrix = graphToCheck.getAdjacencyMatrix();
		// Create a queue (FIFO) of vertex numbers and
		// enqueue source vertex for BFS traversal
		ConcurrentLinkedQueue<Integer> queue = new ConcurrentLinkedQueue<Integer>();
		queue.add(startVertices);

		// Run while there are vertices in queue
		// (Similar to BFS)
		while (!queue.isEmpty()) {
			// Dequeue a vertex from queue
			int u = queue.poll();

			// Return false if there is a self-loop
			if (adjacencyMatrix[u][u] > 0)
				return false;

			// Find all non-colored adjacent vertices
			for (int v = 0; v < numberOfVertices; ++v) {
				// An edge from u to v exists and
				// destination v is not colored
				if (adjacencyMatrix[u][v] > 0 && colorArray[v] == 0) {
					// Assign alternate color to this
					// adjacent v of u
					colorArray[v] = -colorArray[u];
					queue.add(v);
				}

				// An edge from u to v exists and
				// destination v is colored with same
				// color as u
				else if (adjacencyMatrix[u][v] > 0 && colorArray[u] == colorArray[v])
					return false;
			}
		}

		// If we reach here, then all adjacent vertices
		// can be colored with alternate color
		return true;
	}

}
